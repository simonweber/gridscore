__author__ = 'simonweber'
import unittest
import numpy as np
from .. import plotting

class TestPlot(unittest.TestCase):
    def setUp(self):
        self.spikepositions = np.array([
            [0.1, 0.2], [0.3, 0.4], [0.5, 0.6], [0.7, 0.8], [0.9, 1.0]
        ])
        self.arena_limits = np.array([[0, 1], [0, 1]])
        self.plot = plotting.Plot(spikepositions=self.spikepositions,
                             arena_limits=self.arena_limits)

    def test_set_nonspecified_arguments_to_default(self):
        # self.shell_limits_choice = 'automatic_double'
        # self.cut_off_position = 0.5
        arg_default_dict = dict(
            a='foo', b=0.5, c=1e3, d='bar'
        )
        self.plot.a = 'hello'
        self.plot.c = 'one'
        self.plot.set_nonspecified_attributes_to_default(arg_default_dict)
        self.assertEqual(self.plot.a, 'hello')
        self.assertEqual(self.plot.b, 0.5)
        self.assertEqual(self.plot.c, 'one')
        self.assertEqual(self.plot.d, 'bar')

    def test_spike_idx(self):
        plot = self.plot
        spiketimes = np.array([0.1, 0.4, 1.5, 2.8, 2.9])
        plot_with_spiketimes = plotting.Plot(spikepositions=self.spikepositions,
                             arena_limits=self.arena_limits,
                            spiketimes=spiketimes)
        # If None
        n = None
        expected = np.array([0, 1, 2, 3, 4])
        result = plot.spike_idx(n)
        np.testing.assert_array_equal(expected, result)
        # If an integer is given
        n = 3
        expected = np.array([0, 1, 2])
        result = plot.spike_idx(n)
        np.testing.assert_array_equal(expected, result)
        # If an index array is given
        n = np.array([1, 3])
        expected = n
        result = plot.spike_idx(n)
        np.testing.assert_array_equal(expected, result)
        # If a single time interval is given
        n = np.array([[0.4, 2.85]])
        expected = np.array([1, 2, 3])
        result = plot_with_spiketimes.spike_idx(n)
        np.testing.assert_array_equal(expected, result)
        # If several time intervals are given
        n = np.array([[0.2, 0.3], [1.4, 2.85]])
        expected = np.array([2, 3])
        result = plot_with_spiketimes.spike_idx(n)
        np.testing.assert_array_equal(expected, result)
        # If intervals are given, but spiketimes are None.
        n = np.array([[0.2, 0.3], [1.4, 2.85]])
        self.assertRaises(ValueError, plot.spike_idx, n)

    def test_time_intervals_escobar(self):
        self.plot.computed = {}
        self.plot.computed['baselines'] = np.array(
            [
                [3.0, 0.0, 59.1],
                [2.0, 120.1, 200.0]
            ]
        )
        self.plot.computed['trials'] = np.array(
            [
                [2.0, 70., 80.],
                [-2.0, 80., 90.],
                [3.0, 90., 100.],
                [-3.0, 100., 110.],
                [2.0, 110., 120.],
                [-2.0, 120., 130.],
            ]
        )
        # Baseline start
        expected = np.array([[0.0, 59.1]])
        result = self.plot.get_timewindows_of_trial_type(trial='baseline_start')
        np.testing.assert_array_equal(expected, result)
        # Light 2: l2
        expected = np.array([
            [70., 80.], [110., 120.]
        ])
        result = self.plot.get_timewindows_of_trial_type(trial='l2')
        np.testing.assert_array_equal(expected, result)

    def test_concatenate_time_intervals(self):
        self.plot.computed = {}
        self.plot.computed['baselines'] = np.array(
            [
                [3.0, 0.0, 59.1],
                [2.0, 120.1, 200.0]
            ]
        )
        self.plot.computed['trials'] = np.array(
            [
                [2.0, 70., 80.],
                [-2.0, 80., 90.],
                [3.0, 90., 100.],
                [-3.0, 100., 110.],
                [2.0, 110., 120.],
                [-2.0, 120., 130.],
            ]
        )
        # Test for a single trial specification: l2 only
        trials = ['l2']
        expected = np.array(
            [
                [70., 80.], [110., 120.]
            ]
        )
        result = self.plot.concatenate_timewindows(trials=trials)
        np.testing.assert_array_equal(expected, result)
        # Concatenate l2 and d2
        # NB: This leads to first all l2 and then all d2 time windows.
        trials = ['l2', 'd2']
        expected = np.array(
            [
                [70., 80.], [110., 120.], [80., 90.], [120., 130.]
            ]
        )
        result = self.plot.concatenate_timewindows(trials=trials)
        np.testing.assert_array_equal(expected, result)

    def test_shift_timewindows_to_relative_timewindows(self):
        timewindows = np.array(
            [
                [0., 1.], [1., 2.], [1.5, 2.5], [4., 6.]
            ]
        )
        expected = np.array(
            [
                [0., 1.], [0., 1.], [0., 1.], [0., 2.]
            ]
        )
        result = self.plot.shift_timewindows_to_relative_timewindows(
            timewindows)
        np.testing.assert_array_equal(expected, result)

    def test_crop_intervals(self):
        intervals = np.array([
            [0., 100.], [100., 200.], [300., 500.]
        ])
        # First 10
        start = 0
        length = 10
        expected = np.array([
            [0., 10.], [100., 110.], [300., 310.]
        ])
        result = self.plot.crop_intervals(intervals, start, length)
        np.testing.assert_array_equal(expected, result)
        # 10 to 20
        start = 10
        length = 10
        expected = np.array([
            [10., 20.], [110., 120.], [310., 320.]
        ])
        result = self.plot.crop_intervals(intervals, start, length)
        np.testing.assert_array_equal(expected, result)

    def test_get_time_blocks(self):
        resolution = 30
        total_trial_length = 120
        expected = np.array([
            [0, 30], [30, 60], [60, 90], [90, 120]
        ])
        result = self.plot.get_time_blocks(resolution, total_trial_length)
        np.testing.assert_array_equal(expected, result)

    def test_get_gridscores_associated_with_timewindowss(self):
        gridscores = np.array([0.2, 0.3, 0.4, 0.5, 0.6])
        spiketimes = np.array([2.2, 2.3, 2.4, 5.5, 7.6])
        timewindows = np.array([
            [2.3, 3], [3, 5], [7, 9]
        ])
        expected = [
            np.array([0.3, 0.4]),
            np.array([]),
            np.array([0.6])
        ]
        # Only returning the gridscore in timewindows
        result = self.plot.get_gridscores_associated_with_timewindows(gridscores,
                                                                      spiketimes,
                                                                      timewindows)
        for a, b in zip(expected, result):
            np.testing.assert_array_equal(a, b)

    def test_get_times_associated_with_timewindows(self):
        spiketimes = np.array([2.2, 2.3, 2.4, 5.5, 7.6])
        timewindows = np.array([
            [2.3, 3], [3, 5], [7, 9]
        ])
        expected = np.array([
            np.array([2.3, 2.4]),
            np.array([]),
            np.array([7.6])
        ])
        result = self.plot.get_times_associated_with_timewindows(spiketimes, timewindows)
        for a, b in zip(expected, result):
            np.testing.assert_array_equal(a, b)

    def test_get_sorted_times_in_timewindows_flat(self):
        times = np.array([2.2, 2.4, 5.5, 2.3, 7.6])
        intervals = np.array([
          [3, 5], [7, 9], [2.3, 3]
        ])
        expected = np.array([2.3, 2.4, 7.6])
        result = self.plot.get_sorted_times_in_timewindows_flat(times, intervals)
        np.testing.assert_array_equal(expected, result)

    def test_get_sorted_gridscores_in_timewindows_flat(self):
        times = np.array([2.2, 2.4, 5.5, 2.3, 7.6])
        gridscores = np.array([0.2, 0.4, 0.5, 0.3, 0.6])
        intervals = np.array([
          [3, 5], [7, 9], [2.3, 3]
        ])
        expected = np.array([0.3, 0.4, 0.6])
        result = self.plot.get_sorted_gridscores_in_timewindows_flat(times,
                                                                     intervals,
                                                                     gridscores)
        np.testing.assert_array_equal(expected, result)

    def test_get_shift_for_intervals(self):
        intervals = np.array([
            [2.3, 3], [4, 6], [6, 6.5],  [7, 9]
        ])
        expected = np.array(
            [-2.3, -3.3, -3.3, -3.8]
        )
        result = self.plot.get_shift_for_timewindows(intervals)
        np.testing.assert_array_equal(expected, result)

    def test_get_times_in_contiguous_intervals(self):
        intervals = np.array([
            [4, 6], [2.3, 3], [6, 6.5],  [7, 9]
        ])
        spiketimes = np.array([2.3, 2.4, 5.5, 6.3, 7.6])
        expected = np.array([
            0, 0.1, 2.2, 3.0, 3.8
        ])
        result = self.plot.get_times_in_contiguous_intervals(spiketimes,
                                                             intervals)
        np.testing.assert_array_almost_equal(expected, result, decimal=8)

    def test_get_running_average_timewindows(self):
        start = 0.
        end = 320.2
        size = 300
        dt = 5
        # We tant the end time to be included
        expected = np.array([
            [0., 300], [5, 305], [10, 310], [15, 315], [20, 320], [25, 325]
        ])
        result = self.plot.get_running_average_timewindows(start,
                                                           end,
                                                           size, dt)
        np.testing.assert_array_almost_equal(expected, result, decimal=8)

    def test_is_light_time(self):
        times = np.array([0.1, 0.2, 0.3, 0.4])
        light_windows = np.array([
            [0.1, 0.15],
            [0.18, 0.22],
            [0.35, 0.45]
        ])
        expected = np.array([True, True, False, True])
        result = self.plot.is_light_time(times, light_windows)
        np.testing.assert_array_almost_equal(expected, result)

    def test_create_light_windows(self):
        n = 4
        size = 120
        expected = np.array([
            [0, 120], [240, 360], [480, 600], [720, 840]
        ])
        result = self.plot.create_light_windows(n, size)
        np.testing.assert_array_almost_equal(expected, result)

    def test_get_mode_from_parameters(self):
        self.plot.n_symmetry = 6
        self.plot.compare_to_other_symmetries = True
        self.plot.shell_limits_choice = 'automatic_single'
        self.plot.spike_selection = None
        # # Do not use pre computed values
        # from_computed = False
        # expected = None
        # result = self.plot.get_mode_from_parameters(from_computed)
        # self.assertEqual(expected, result)
        # The default scenario
        from_computed = True
        expected = 'each_spike_final'
        result = self.plot.get_mode_from_parameters()
        self.assertEqual(expected, result)
        # Do not compare to other symmetries
        self.plot.compare_to_other_symmetries = False
        expected = 'each_spike_final_no_cosy'
        result = self.plot.get_mode_from_parameters()
        self.assertEqual(expected, result)
        # Only high velocities
        self.plot.compare_to_other_symmetries = True
        self.plot.spike_selection = 'velocity_above_15'
        expected = 'each_spike_final_velocity_15'
        result = self.plot.get_mode_from_parameters()
        self.assertEqual(expected, result)
        # High velocity and hard coded shell limits
        self.plot.shell_limits_choice = '130'
        expected = 'each_spike_final_velocity_15_shell_130'
        result = self.plot.get_mode_from_parameters()
        self.assertEqual(expected, result)
        # 7 fold symmetry
        self.plot.n_symmetry = 7
        self.plot.shell_limits_choice = 'automatic_single'
        self.plot.spike_selection = None
        expected = 'each_spike_final_n_7'
        result = self.plot.get_mode_from_parameters()
        self.assertEqual(expected, result)
