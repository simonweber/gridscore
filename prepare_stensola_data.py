import numpy as np
import matlab.engine
import os
import glob

maindir = '/Users/simonweber/doktor/Data/2012_Stensola'
os.chdir(maindir)
filepaths = glob.glob(maindir + '/recordings/*.h5')
eng = matlab.engine.start_matlab()
for fp in filepaths:
    print(fp)
    eng.readNwb_modified(fp, nargout=0)