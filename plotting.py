import math
import os

import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
from scipy import stats
from scipy import signal
from scipy import ndimage

from .arrays import find_nearest
from .arrays import get_maximapositions_maximavalues
from .correlogram import Gridness
from .correlogram import pearson_correlate2d
from .dictionaries import mode_arguments
from .dictionaries import str_to_int_escobar
from .plot_settings import *
from .plot_settings import trial_colors
from .spikes import Spikes
from .arrays import intervals2centers
from .arrays import paths_for_rectangular_partitioning
from .arrays import paths_for_diagonal_partitioning
from matplotlib import gridspec
from gridscore.head_direction import Head_Direction_Tuning

import matplotlib.patches as patches

ARGUMENTS_DEFAULT = dict(
    shell_limits_choice='automatic_single',
    neighborhood_size_as_fraction=0.1,
    threshold_difference_as_fraction=0.1,
    cut_off_position=0,
    maximum_index=1,
    bins=200,
    weights=None,
    gridscore_norm=None,
    compare_to_other_symmetries=True,
    std_threshold=None,
    n_symmetry=6,
    axis=None,
)


def simpleaxis(ax):
    """
    Creates an axis with spines only left and bottom

    Taken from example idn stackoverflow
    """
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    ax.get_xaxis().tick_bottom()
    ax.get_yaxis().tick_left()


def plot_list(fig, plot_list, automatic_arrangement=True):
    """
    Takes a list of lambda forms of plot functions and plots them such that
    no more than four rows are used to assure readability

    Note: Instead of lambda forms, you can also use functools.partial to
    pass the not yet evaluated functions.
    """
    n_plots = len(plot_list)
    # A title for the entire figure (super title)
    # fig.suptitle('Time evolution of firing rates', y=1.1)
    if automatic_arrangement:
        for n, p in enumerate(plot_list, start=1):
            # Check if function name contains 'polar'
            # is needed for the sublotting.
            # A silly hack, that only works if every function that should
            # use polar plotting actually has the string 'polar' in its
            # name.
            if 'polar' in str(p.func):
                polar = True
            else:
                polar = False
            if n_plots < 4:
                fig.add_subplot(n_plots, 1, n, polar=polar)
            # plt.locator_params(axis='y', nbins=4)
            # plt.ylabel('firing rate')
            else:
                fig.add_subplot(math.ceil(n_plots / 2.), 2, n, polar=polar)
            # plt.locator_params(axis='y', nbins=4)
            p()


class Plot:
    def __init__(self, tables=None, psps=[None],
                 spikepositions=None, arena_limits=None,
                 spiketimes=None,
                 noise=0,
                 latex=None,
                 frame_slice=slice(None, None, None),
                 spikeheaddirections=None,
                 headdirections=None):
        self.spike_selection = None
        self.shell_limits_choice = 'automatic_single'
        self.psps = psps
        self.cmap = getattr(mpl.cm, 'viridis')
        self.tables = tables
        if self.tables is not None:
            # Works only if single psp is given
            self.params_and_rawdata_is_given = False
            self.set_params_rawdata_computed(psps[0])
            self.rawdata = self.plotsnep.rawdata
            try:
                self.computed = self.plotsnep.computed
            except AttributeError:
                self.computed = None
            self.spikepositions = self.plotsnep.rawdata['spikepositions'][
                frame_slice]
            self.spiketimes = self.plotsnep.rawdata['spiketimes'][frame_slice]
            self.arena_limits = self.plotsnep.rawdata['arena_limits']
            try:
                self.spikeheaddirections = self.plotsnep.rawdata[
                    'spikeheaddirections']
                self.headdirections = self.plotsnep.rawdata[
                    'headdirections']
            except:
                self.spikeheaddirections = None
                self.headdirections = None
            self.ratpositions = self.plotsnep.rawdata['ratpositions']
            self.positiontimes = self.plotsnep.rawdata['positiontimes']
        else:
            self.spikepositions = spikepositions
            self.arena_limits = arena_limits
            self.spiketimes = spiketimes
            self.spikeheaddirections = spikeheaddirections

        self.spikes = Spikes(positions=self.spikepositions,
                             arena_limits=self.arena_limits,
                             times=self.spiketimes)

    def set_params_rawdata_computed(self, psp, set_sim_params=False):
        import general_utils.snep_plotting
        self.plotsnep = general_utils.snep_plotting.Snep()
        self.plotsnep.tables = self.tables
        return self.plotsnep.set_params_rawdata_computed(psp=psp,
                                                         set_sim_params=set_sim_params)

    def test_plot(self):
        plt.plot(np.arange(5))

    def gridscore_time_evolution(self, method='automatic_single_stdt1',
                                 ylim_conventional=(-0.7, 1.5),
                                 ylim_psi=(0, 0.4), color=None,
                                 every_nth_spikenumber=500,
                                 n_spikes_label=True,
                                 legend=True, time_label=True,
                                 xlim=None,
                                 xticks=None):
        """
        The time evolution of grid scores.

        Parameters
        ----------
        method : str {'langston', 'automatic_single_stdt1', ...}
            Method of the grid score whos time evolution is plotted.
            See computed.py for more.
        ylim_conventional : 2-tuple
            The y limits for conventional grid scores
        ylim_psi : 2-tuple
            The y limits for PSI based grid scores
        every_nth_spikenumber : int
            Multiple at which the number of spikes are indicated.
        Returns
        -------
        """
        color_psi = color_cycle_blue3[0]
        gridscores = self.computed['gridscores'][method]
        ax1 = plt.gca()
        ###############################################
        #### ax2 shows Langston or sargolini score ####
        ###############################################
        if (('sargolini' in method) or ('langston' in method)):
            if color:
                color = color
            else:
                color = color_cycle_blue3[2]
            # Conventional scores have the y axis on the right and use
            # a different scale (ylim)
            ax3 = ax1.twinx()
            plt.setp(ax3,
                     ylim=ylim_conventional,
                     xlabel='Time [s]',
                     ylabel='Langston',
                     yticks=[0, 1]
                     )
            times = gridscores[:, 0]
            gs = gridscores[:, 1]
            ax3.plot(times, gs, label=time_evo_labels[method], lw=2,
                     linestyle='solid', color=color)
            if legend:
                ax3.legend(loc='upper left', frameon=False)
            for ytick in ax3.get_yticklabels():
                ytick.set_color(color)
            ax3.yaxis.label.set_color(color)
        else:
            ###############################################
            ##########  ax2 shows PSI grid score ##########
            ###############################################
            times = gridscores[:, 0]
            gs = gridscores[:, 1]
            ax1.plot(times, gs,
                     label=time_evo_labels[method], lw=2,
                     zorder=100,
                     color=color_psi)
            if legend:
                ax1.legend(loc='upper right', frameon=False)
            plt.setp(ax1,
                     xlim=[0, self.spiketimes[-1]],
                     ylim=ylim_psi,
                     ylabel=r'$\Psi$ score',
                     yticks=[0, 0.3],
                     )
            if xticks:
                plt.xticks(xticks)
            if time_label:
                ax1.set_xlabel('Time [s]')
            # ax1.locator_params(axis='x', tight=True, nbins=3)
            for ytick in ax1.get_yticklabels():
                ytick.set_color(color_psi)
            ax1.yaxis.label.set_color(color_psi)
            ###############################################
            ###### Top x-axis shows number of spikes ######
            ###############################################
            # The number of spikes is indicated on the x-axis at the top
            # NB: The experimental time and the number of spikes are not
            # linearly aligned.
            ax2 = ax1.twiny()
            ax2.set_xlim(ax1.get_xlim())
            spiketimes, spikenumbers = self.spikes.get_spiketimes_spikenumbers(
                every_nth=every_nth_spikenumber, include_zero=True
            )
            spikenumbers_in_hundreds = spikenumbers / 100
            spikenumbers_in_hundreds = spikenumbers_in_hundreds.astype(
                np.int64)
            plt.setp(ax2,
                     ylabel=r'$\Psi$ score',
                     xticks=spiketimes,
                     xticklabels=spikenumbers_in_hundreds
                     )
            if n_spikes_label:
                ax2.set_xlabel('# Spikes [in hundreds]')
            # ax2.locator_params(axis='x', tight=True, nbins=3)
            for xtick in ax2.get_xticklabels():
                xtick.set_color('0.4')
            ax2.xaxis.label.set_color('0.4')
            # Set the current axis to ax1 again, to allow for adding plots
            # without messing with the labels
            plt.sca(ax1)

    def _print_trial_time_information(self):
        print('Spike number closest to half the trial time:')
        center_idx = find_nearest(self.spiketimes, self.spiketimes[-1] / 2,
                                  ret='index')
        print(center_idx + 1)
        print('Spike time closest to half the trial time:')
        center_time = find_nearest(self.spiketimes, self.spiketimes[-1] / 2,
                                   ret='element')
        print(center_time)
        print('Total number of spikes:')
        print(len(self.spikepositions))

    ###########################################################################
    ################## Gridscore time evolution Escobar 2016 ##################
    ###########################################################################

    def get_gridscores_associated_with_timewindows(self, gridscores, spiketimes,
                                                   timewindows, start_time_shift=0,
                                                   end_time_shift=0):
        """
        Gridscores within time windows.

        NB: This is different from _gridscores_in_timewindows in computed.py.
        See documentation there.

        Parameters
        ----------
        gridscores : ndarray of shape (N)
            The grid scores of N spikes
        spiketimes : ndarray of shape (N)
            The spike times of the N spikes
        timewindows : ndarray of shape (M, 2)
            M timewindows like [start_time, end_time]
        start_time_shift : float
        end_time_shift : float

        Returns
        -------
        gs_in_tw : list of ndarrays
            [gridscores_in_first_timewindow,
            gridscores_in_second_timewindow, ...]
        """
        gs_in_tw = []
        for tw in timewindows:
            condition = np.logical_and(tw[0] + start_time_shift <= spiketimes,
                                       spiketimes < tw[1] + end_time_shift)
            gridscores_within_window = gridscores[condition]
            gs_in_tw.append(gridscores_within_window)
        return gs_in_tw

    def get_times_associated_with_timewindows(self, times, timewindows):
        """
        Spiketimes within timewindows.

        Bascially the same as get_gridscores_in_timewindows.
        Sort of trivial here, but it's convenient to have the same structure.

        Parameters
        ----------
        See get_gridscores_in_timewindows

        Returns
        -------
        list of ndarrays
            [spiketimes_in_first_timewindow,
            spiketimes_in_second_timewindow, ...]
        """
        spiketimes_in_tw = []
        for tw in timewindows:
            condition = np.logical_and(tw[0] <= times, times < tw[1])
            spiketimes_in_tw.append(times[condition])
        return spiketimes_in_tw

    def get_spiketimes_and_gridscores_in_trials_sorted(self, trials=('l2', 'd2'),
                                                       mode='in_l2d2',
                                                       make_contiguous=False):
        """
        Returns all spiketimes and gridscores in multiple trials

        Returns
        -------
        tuple of 2 ndarrays of shape (N)
        """
        spiketimes = self.get_spiketimes_in_mode(mode=mode)
        gridscores = self.computed['gridscores'][mode]
        timewindows = self.concatenate_timewindows(trials)
        t = self.get_sorted_times_in_timewindows_flat(spiketimes, timewindows)
        gs = self.get_sorted_gridscores_in_timewindows_flat(spiketimes,
                                                            timewindows,
                                                            gridscores)
        if make_contiguous:
            t = self.get_times_in_contiguous_intervals(t, timewindows)
        return t, gs

    def get_times_in_contiguous_intervals(self, times, timewindows):
        """
        Returns shifted times, such that underlying intervals would be
        contiguous

        Parameters
        ----------
        times : ndarray of shape (K)
            Typically spiketimes. Note that all times need to be within the
            given time windows.
        timewindows : ndarray of shape (N, 2)
        Returns
        -------
        shifted_times : ndarray of shape (K)
        """
        times_sorted = np.sort(times)
        timewindows_sorted = np.sort(timewindows, axis=0)
        shifts = self.get_shift_for_timewindows(timewindows_sorted)
        times_in_timewindows = self.get_times_associated_with_timewindows(
            times_sorted, timewindows_sorted)
        shifted_times = []
        for t, s in zip(times_in_timewindows, shifts):
            shifted_times.append(t + s)
        return np.concatenate(tuple(shifted_times))

    @staticmethod
    def get_shift_for_timewindows(timewindows, start_zero=True):
        """
        Returns shift value for each time windows to make them contingent

        Given a set of non-overlapping timewindows with temporal gaps between
        timewindows, applying the returned shift would lead to non-overlapping
        gaps without temporal gaps.

        NB: The shifts are negative.

        Parameters
        ----------
        timewindows : ndarray of shape (N, 2)
            NB: The time windows must be sorted by time.
        start_zero : bool
            If True, the first time windows is shifted to start at 0.

        Returns
        -------
        shifts : ndarray of shape (N)
            Shift value for each time windows
        """
        # Set first shift to 0
        shifts = [0]
        # Iterate over all time windows
        for n in np.arange(0, len(timewindows) - 1, 1):
            # Temporal gap between two consecutive intervals
            gap = timewindows[n, 1] - timewindows[n + 1, 0]
            shifts.append(gap)
        # In order to be contingent
        shifts = np.cumsum(shifts)
        # Shift first time to 0
        if start_zero:
            shifts -= timewindows[0, 0]
        return shifts

    def get_sorted_times_in_timewindows_flat(self, times, timewindows,
                                             return_sort_idx=False):
        """
        Returns spiketimes in timewindows, sorted by time

        Parameters
        ----------
        spiketimes : ndarray of shape (N)
        timewindows : ndarray of shape (M, 2)

        Returns
        -------
        ndarray of shape (K)
            K <= N, because not all spiketimes will be in the timewindows
        """
        times_in_timewindows = np.concatenate(
            tuple(
                self.get_times_associated_with_timewindows(times, timewindows=timewindows)
            )
        )
        idx = np.argsort(times_in_timewindows)
        if return_sort_idx:
            return idx
        else:
            return times_in_timewindows[idx]

    def get_sorted_gridscores_in_timewindows_flat(self, times, timewindows,
                                                  gridscores):
        """
        Returns gridscores of spikes in timewindows, sorted by time of spike

        Parameters
        ----------
        spiketimes : ndarray of shape (N)
        gridscores : ndarray of shape (N)
        timewindows : ndarray of shape (M, 2)

        Returns
        -------
        ndarray of shape (K)
            K <= N, because not all spiketimes will be in the timewindows
        """
        idx = self.get_sorted_times_in_timewindows_flat(times, timewindows,
                                                        return_sort_idx=True)
        gridscores_in_trials = np.concatenate(
            tuple(
                self.get_gridscores_associated_with_timewindows(gridscores, times,
                                                                timewindows=timewindows)
            )
        )
        return gridscores_in_trials[idx]

    def get_spiketimes_in_mode(self, mode='in_l3d3'):
        """"Spike times of all spikes that are part of a mode"""
        # Get the time intervals for all trials in the mode
        intervals_mode = self.get_timewindows_from_mode(mode)
        # Index into all spikes that are part of the mode
        idx_mode = self.spike_idx(intervals_mode)
        # Get the spiketimes of all spikes that are part of the mode
        return self.spiketimes[idx_mode]

    def plot_gridscore_in_escobar_trial(self, mode='in_l3d3',
                                        trial='l3'):
        # Get the grid scores for the given mode
        gridscores = self.computed['gridscores'][mode]
        # Get the spike time for the given mode
        spiketimes = self.get_spiketimes_in_mode(mode)
        # Get the time intervals for the trial type that is to be plotted
        intervals_trial = self.get_timewindows_of_trial_type(trial)
        # Get the gridscores in the timewindows of that trial
        gs_in_tw = self.get_gridscores_associated_with_timewindows(gridscores,
                                                                   spiketimes,
                                                                   intervals_trial)
        # Plot the time windows and the gridscores within
        self.plot_gridscore_in_intervals(intervals_trial, gs_in_tw,
                                         color=trial_colors[trial], label=trial)

    def get_stats_in_intervals(self, gridscores_in_intervals, stats='mean'):
        """
        Grid score statistics within time intervals

        Parameters
        ----------
        gridscores_in_intervals : list of length N with ndarrays of different
        size elements.

        Returns
        -------
        ret : ndarray of shape (N)
            The statistical value of the grid scores within each interval
        """
        gs_stats = []
        for gs in gridscores_in_intervals:
            gs_stats.append(self._stats(gs, sttstcs=stats))
        return np.array(gs_stats)


    def plot_gridscore_in_intervals(self, intervals, gridscores_in_intervals,
                                    stats='mean', color=color_cycle_blue3[0],
                                    label=None,
                                    plot_type='bar'):
        """
        Plots the time intervals along x and the gridscore statistics along y

        Parameters
        ----------
        intervals : ndarray of shape (N, 2)
        gridscores_in_intervals : list of length N of one dimensional
                    ndarrays of different length.
        stats : {'mean'}
            How to plot the grid score in each interval.
        """
        simpleaxis(plt.gca())
        if plot_type == 'bar':
            gs_stats = self.get_stats_in_intervals(gridscores_in_intervals,
                                                   stats=stats)
            widths = intervals[:, 1] - intervals[:, 0]
            plt.bar(intervals[:, 0], gs_stats, width=widths, align='edge',
                color=color, label=label)
        elif plot_type == 'box':
            props = dict(color=color_cycle_blue3[1])
            flierprops = dict(
                alpha=0.2,
                # markeredgecolor='red',
                marker='o',
                markeredgewidth=0,
                markersize=1)
            medianprops = dict(color=color_cycle_blue3[0], lw=10)
            plt.boxplot(gridscores_in_intervals.tolist(),
                        boxprops=props, whiskerprops=props,
                        capprops=props, flierprops=flierprops,
                        medianprops=medianprops
                        )
        elif plot_type == 'line':
            gs_stats = self.get_stats_in_intervals(gridscores_in_intervals,
                                                   stats=stats)
            centers = intervals2centers(intervals)
            plt.plot(centers, gs_stats, color=color)
        else:
            gs_stats = self.get_stats_in_intervals(gridscores_in_intervals,
                                                   stats=stats)
            centers = intervals2centers(intervals)
            plt.plot(centers, gs_stats, color=color, marker='o')
            # plt.plot(centers, gs_stats, color=color)

        # Plot settings
        plt.legend(loc='upper right',
                   # bbox_to_anchor=(1,1),
                   numpoints=1,
                   fontsize=12,
                   frameon=False,
                   handlelength=0.5, ncol=2)
        ax = plt.gca()
        plt.setp(ax,
                 xlabel='Time [s]',
                 ylabel=gridscore_sliding_window_str)
        # ax.grid(axis='y', linestyle='--', color='gray', lw=0.75)

    # def plot_gridscores_in_trial_intervals_averaged_over_cells(self, trial,
    #                                     mode, identifiers):
    #     # Iterate over cells (identifiers)
    #     means_in_intervals = []
    #     # self.computed_full
    #     for i in identifiers:
    #         self.computed['mean_gridscore_in_trial_intervals'][trial][mode]

    def plot_decay_escobar(self, mode='in_l3d3', trial='l3',
                           resolution=10, total_trial_length=120):
        """
        Plots the decay of the grid with high temporal resolution.

        Parameters
        ----------
        mode : str
        trial : str
            See other functions
        resolution : float
            Temporal resolution in seconds
        total_trial_length : float
            Duration of a single light on or off trial
        """
        # The intervals that divide a single trial length with given resolution
        time_blocks = self.get_time_blocks(resolution,
                                    total_trial_length=total_trial_length)
        gs_in_blocks = self.get_gridscores_in_timeblocks(trial, mode,
                                                         time_blocks)
        self.plot_gridscore_in_intervals(time_blocks, gs_in_blocks,
                                         color=trial_colors[trial])

    def get_gridscores_in_timeblocks(self, trial, mode, time_blocks):
        """
        Returns gridscores of spikes in time blocks within trials

        Specifically written to analyze Escobar 2016 data.

        Parameters
        ----------
        trial : str
            The trial, for which time slices are cropped.
            E.g., trial='l2' means that each l2 trial is separated into time
            blocks and the gridscores within each of the blocks (for all
            instances of the l2 trial type) are returned.
        mode : str (see documentation elsewhere)
        time_blocks : ndarray of shape (N, 2)
            Start and end time of the intervals.

        Returns
        -------
        gridscores_in_timeblocks : list of ndarray
            The list has N elements. Each element is an ndarray. Different
            ndarrays can have different length.
        """
        # Get the resolution from the width of the first time block
        resolution = time_blocks[1, 0] - time_blocks[0, 0]
        # Get the grid scores for the given mode
        gridscores = self.computed['gridscores'][mode]
        # Get the spike times for the given mode
        spiketimes = self.get_spiketimes_in_mode(mode)
        # Get the time intervals for the given trial type
        intervals_trial = self.get_timewindows_of_trial_type(trial)
        # The grid scores of all spikes within the temporal blocks (for each
        # iteration of the specified trial)
        gs_in_blocks = []
        for start in time_blocks[:, 0]:
            # Get cropped intervals, so a temporal crop of each iteration of
            # the specified trial.
            cropped_intervals = self.crop_intervals(
                intervals_trial, start=start, length=resolution)
            # Get the gridscores in all of theses blocks
            gs_in_cropped_intervals = self.get_gridscores_associated_with_timewindows(
                gridscores, spiketimes, cropped_intervals)
            # Concatenate them, because they correspond to the same time block
            gs_in_blocks.append(
                np.concatenate(tuple(gs_in_cropped_intervals)))
        return gs_in_blocks

    def _stats(self, values, sttstcs='mean'):
        """Convenience function to check different statistics"""
        values_without_nan = values[~np.isnan(values)]
        if sttstcs == 'mean':
            return np.mean(values_without_nan)
        elif sttstcs == 'circmean':
            high = 30
            low = -30
            return stats.circmean(values_without_nan, high=high, low=low)
        elif sttstcs == 'circstd':
            high = 30
            low = -30
            return stats.circstd(values_without_nan, high=high, low=low)
        elif sttstcs == 'circsem':
            high = 30
            low = -30
            circstd = stats.circstd(values_without_nan, high=high, low=low)
            return circstd / np.sqrt(len(values))
    def plot_escobar_trials(self, mode='each_spike_final_l3d3', trials='l3'):
        """
        Plot gridscore of different escobar trials in a single plot

        See `plot_gridscore_in_escobar_trial`.
        """
        simpleaxis(plt.gca())
        for trial in np.atleast_1d(trials):
            self.plot_gridscore_in_escobar_trial(mode=mode, trial=trial)

    @staticmethod
    def get_time_blocks(resolution, total_trial_length=120):
        starts = np.arange(0, total_trial_length, resolution)
        ends = starts + resolution
        return np.hstack((starts[:, np.newaxis], ends[:, np.newaxis]))

    @staticmethod
    def crop_intervals(intervals, start=0, length=10):
        """
        Returns a temporal crop for each given interval

        Parameters
        ----------
        intervals : ndarray of shape (N, 2)
            Time intervals
        start : float
            The start time (relative to the start time in the interval)
        length : float
            The length of the cropping window
        Returns
        -------
        croped intervals : ndarray of shape (N, 2)
            A new intervals array

        Examples
        --------
        >>> intervals =  np.array([[0., 100.], [100., 200.], [300., 500.]])
        >>> start=10
        >>> length=10
        >>> plot.crop_intervals(intervals, start, length)
        np.array([[10., 20.], [110., 120.], [310., 320.]])

        """
        start_times =  intervals[:, 0, np.newaxis] + start
        end_times = intervals[:, 0, np.newaxis] + start + length
        return np.hstack((start_times, end_times))

    def gridscore_each_spike_running_average(self, mode='in_l2d2',
                                            trials=('l2', 'd2'),
                                            make_contiguous=True,
                                            size=120,
                                            dt=120):
        spiketimes, gridscores = \
            self.get_spiketimes_and_gridscores_in_trials_sorted(
                trials, mode, make_contiguous=make_contiguous)

        end = np.ceil(spiketimes[-1] / dt) * dt
        running_average_windows = self.get_running_average_timewindows(
            start=0, end=end, size=size, dt=dt)
        gs = self.get_gridscores_associated_with_timewindows(gridscores,
                        spiketimes, timewindows=running_average_windows)
        mean_gs_in_windows = self.get_stats_in_intervals(gs)
        plt.plot(running_average_windows[:, 0], mean_gs_in_windows,
                 marker='o')

    @staticmethod
    def get_running_average_timewindows(start=0., end=1200.,
                                        size=300., dt=5.):
        """
        Returns time windows for a sliding average

        Parameters
        ----------


        Returns
        -------
        """
        start_times = np.arange(start, (end - size) + dt, dt)
        end_times = start_times + size
        timewindows = np.hstack(
                        (start_times[:, np.newaxis],
                          end_times[:, np.newaxis])
        )
        return timewindows

    def gridscore_time_evolution_each_spike(self,
                                            mode='in_l2d2',
                                            trials=('l2', 'd2'),
                                            make_contiguous=True,
                                            data='escobar'):
        color = color_cycle_blue3[0]
        if data == 'escobar':
            spiketimes, gridscores = \
                self.get_spiketimes_and_gridscores_in_trials_sorted(
                trials, mode, make_contiguous=make_contiguous
            )
        else:
            gridscores = self.computed['gridscores']['each_spike_final']
            spiketimes = self.rawdata['spiketimes']
        # Plot the spike times and the grid score of the associated spike
        plt.plot(spiketimes, gridscores, color=color, linestyle='none',
                 marker='o', markersize=1, clip_on=False)
        # Print information relevant for trials where the underlying
        # distribution was changed after half the trial time.
        # self._print_trial_time_information()

        # Plot settings
        ax = plt.gca()
        ymax = max([0.4, max(gridscores)]) * 1.05
        plt.setp(ax,
                 xlim=[0, self.spiketimes[-1]],
                 ylim=[0, ymax],
                 ylabel=gridscore_str,
                 yticks=np.arange(0, 1, 0.2),
                 # xticklabels=[]
                 )
        # ax.grid(axis='y', linestyle='--', color='gray', lw=0.75)
        plt.locator_params(axis='y', tight=True, nbins=3)
        simpleaxis(ax)

    def _plot_bar_in_bin(self, timewindows, means, color=color_cycle_red3[0]):
        """
        Plots barplot of light and dark trials separated

        Also plots horizontal line for mean of all light and mean of all dark
        trials.

        Parameters
        ----------
        timewindows : ndarray of shape (N, 2)
            Time windows in which spikes were binned.
        means : ndarray of shape (N,)
            The mean grid score within each time window.
        trials : str {None, 'light', 'dark'}
            Selects which trials to show (a selection of the time windows).
            If None, all time windows are considered.
        """
        # if trial == 'light':
        #     # The time windows for the light trials are the first N/2
        #     slc = np.s_[:int(len(timewindows) / 2)]
        #     color = color_cycle_blue3[1]
        # elif trial == 'dark':
        #     # The time windows for the dark trials are the last N/2
        #     slc = np.s_[int(len(timewindows) / 2):]
        #     color = color_cycle_blue3[0]
        # else:
        #     slc = np.s_[:]
        #     color='black'
        tw = timewindows
        m = means
        twindow_starts = tw[:, 0]
        twindow_ends = tw[:, 1]
        widths = twindow_ends - twindow_starts
        plt.bar(twindow_starts, m, width=widths,
                align='edge', color=color, alpha=0.4, lw=0)
        mean_in_trial = np.mean(m)
        plt.axhline(y=mean_in_trial, color=color, lw=1,
                    linestyle='dotted')

    def gridscore_time_evolution_each_spike_final_binned(self,
                                                         mode='each_spike_final',
                                                         data=None,
                                                         n_bins=None):
        # Get the grid scores from mode
        gridscores = self.computed['gridscores'][mode]
        # Get the spike times from mode
        spiketimes = self.get_spiketimes_in_mode(mode)
        # Get the intervals of the given mdoe
        intervals = self.get_timewindows_from_mode(mode)
        if data == 'escobar':
            timewindows = intervals
        else:
            timewindows = self.get_timewindows(n=n_bins,
                        last_spiketime=spiketimes[-1], windowmethod='bins')
        twindow_ends = timewindows[:, 1]

        gs_in_tw = self.get_gridscores_associated_with_timewindows(gridscores,
                                                                   spiketimes,
                                                                   timewindows)
        means = []
        for g in gs_in_tw:
            means.append(np.mean(g))
        means = np.array(means)

        self._plot_bar_in_bin(timewindows, means)
        # self._plot_bar_in_bin(timewindows, means, trial='dark')

        ymax = max([0.3, max(means)])
        ax = plt.gca()
        plt.setp(ax,
                 xlim=[0, self.spiketimes[-1]],
                 ylim=[0, ymax],
                 ylabel=r'$\langle \psi_k \rangle$',
                 yticks=[0, 0.1, 0.2, 0.3, 0.4],
                 # xticks = [0, 500],
                 xlabel='Time [s]'
                 )
        simpleaxis(ax)
        # Mark the ends of the time windows with vertical lines
        # plt.vlines(x=twindow_ends, ymin=0, ymax=ymax, color='gray',
        #            linestyle='--', lw=0.75)
        # Show a grid, to make comparison easier
        ax.grid(axis='y', linestyle='--', color='gray', lw=0.75)
        # Show the overall mean gridscore as a horizontal line
        mean_gridscore = np.mean(gridscores)
        mean_gs_color = 'black'
        plt.axhline(y=mean_gridscore, color=mean_gs_color, lw=1,
                    linestyle='solid')
        plt.text(self.spiketimes[-1] * 1.01, mean_gridscore, r'$\Psi$',
                 color=mean_gs_color, horizontalalignment='left',
                 verticalalignment='center')



    def gridscore_time_evolution_together(self, stepsize=1,
                                          gridscore_norm=None):
        # if gridscore_norm is 'all_neighbor_pairs':
        # 	self.gridscore_time_evolution(
        # method='automatic_single_neighbor_norm',
        # 								  stepsize=stepsize)
        # 	self.gridscore_time_evolution(
        # method='automatic_single_final_neighbor_norm',
        # 								  stepsize=1)
        # 	self.gridscore_time_evolution(
        # method='automatic_single_neighbor_norm_stdt1',
        # 								  stepsize=stepsize)
        # 	self.gridscore_time_evolution(
        # method='automatic_single_final_neighbor_norm_stdt1',
        # 								  stepsize=1)
        # elif gridscore_norm is None:
        # 	# self.gridscore_time_evolution(method='automatic_single',
        # 	# 							  stepsize=stepsize)
        # 	# self.gridscore_time_evolution(method='automatic_single_final',
        # 	# 							  stepsize=1)
        # 	self.gridscore_time_evolution(method='automatic_single_stdt1',
        # 								  stepsize=stepsize)
        # 	self.gridscore_time_evolution(
        # method='automatic_single_final_stdt1',
        # 								  stepsize=1)

        # self.gridscore_time_evolution(method='sargolini',
        # 							  stepsize=stepsize)

        ylim = (0.0, 1.0)
        # self.gridscore_time_evolution(method='window_300', ylim_psi=ylim)
        # self.gridscore_time_evolution(method='window_600', ylim_psi=ylim)
        # self.gridscore_time_evolution(method='window_1200', ylim_psi=ylim)
        # self.gridscore_time_evolution(method='decrease_from_left',
        # ylim_psi=ylim)
        # self.gridscore_time_evolution(method='decrease_from_left_langston',
        # ylim_psi=ylim)
        # self.gridscore_time_evolution(method='window_1200_langston',
        # ylim_psi=ylim)
        # self.gridscore_time_evolution(method='sargolini', ylim_psi=ylim)
        # self.gridscore_time_evolution(method='automatic_single',
        # ylim_psi=ylim)
        self.gridscore_time_evolution(method='window_300', ylim_psi=ylim)
        # self.gridscore_time_evolution(method='window_600', ylim_psi=ylim)
        # self.gridscore_time_evolution(method='decrease_from_left',
        # ylim_psi=ylim)
        self.gridscore_time_evolution(method='window_300_langston',
                                      ylim_psi=ylim, color='black')
        self.gridscore_time_evolution(method='langston', ylim_psi=ylim)

    def determine_shell_limits_and_get_psi_n_all(self, from_computed=False,
                                                 return_shell_limits=False):
        """
        Returns psi_n for all selected spikes

        Either computes psi or returns it from computed values in the .h5 file
        """
        if return_shell_limits or from_computed == False:
            shell_limits = self.spikes.get_shell_limits(
                shell_limits_choice=self.shell_limits_choice,
                neighborhood_size_as_fraction=self.neighborhood_size_as_fraction,
                cut_off_position=self.cut_off_position,
                threshold_difference_as_fraction=self
                    .threshold_difference_as_fraction,
                bins=self.bins, weights=self.weights,
                maximum_index=self.maximum_index)

        if from_computed:
            mode = self.get_mode_from_parameters()
            psi_n = self.polar2rectangular(
                radii=self.computed['gridscores'][mode],
                angles=self.computed['orientations'][mode])
        else:
            psi_n = self.spikes.get_psi_n_all(
                n_symmetry=self.n_symmetry, shell_limits=shell_limits,
                normalization=self.gridscore_norm,
                std_threshold=self.std_threshold,
                compare_to_other_symmetries=self.compare_to_other_symmetries,
                axis=self.axis)
        if return_shell_limits:
            return psi_n, shell_limits
        else:
            return psi_n

    def polar2rectangular(self, radii, angles):
        return radii * np.exp(1j*angles)

    def set_nonspecified_attributes_to_default(self, attribute_default_dict):
        """
        Check if attributs (keys) are defined. If not, set to default (values).

        Parameters
        ----------
        attribute_default_dict : dict
            Keys are attributes of the class.
            Values are their default values.
        """
        for key, value in attribute_default_dict.items():
            try:
                getattr(self, key)
            except AttributeError:
                setattr(self, key, value)

    def get_spikes_class_for_spike_selection(self, spike_selection=None,
                                             return_idx=False):
        """
        Returns a Spikes class that only considers the first n_spikes
        or the spikes specified by an index array.

        Parameters
        ----------
        n_spikes : int
            The number of spikes that should be considered
        idx : Index array that selects spikes

        Returns
        -------
        sc : A Spikes instance
        """
        if self.is_escobar_trial(spike_selection):
            n = self.concatenate_timewindows(
                trials=np.atleast_1d(spike_selection))
        elif spike_selection != None and 'velocity_above' in spike_selection:
            v_threshold_str = spike_selection.split('velocity_above_')[1]
            n = self.computed['timewindows_high_velocity'][v_threshold_str]
        else:
            n = spike_selection
        idx = self.spike_idx(n=n)
        if idx.size == 0:
            return

        try:
            # If spiketimes don't exist, we set them to None
            try:
                times = self.spiketimes[idx]
            except TypeError:
                times = None
            sc = Spikes(positions=self.spikepositions[idx],
                        arena_limits=self.arena_limits,
                        times=times)
        except AttributeError:
            # When used in experiment, there is no rawdata defined yet.
            # This is a hack now.
            # sc = Spikes(positions=self.spikepositions[frame_slice],
            # 				arena_limits=self.arena_limits['arena_limits'],
            # 				times=self.spiketimes['spiketimes'][frame_slice])
            return self.spikes
        if return_idx:
            return idx, sc
        else:
            return sc

    def gridscore_vs_location(self, shell_limits_choice='automatic_single',
                              neighborhood_size_as_fraction=0.1,
                              cut_off_position=0,
                              threshold_difference_as_fraction=0.1,
                              from_computed=False,
                              maximum_index=1):

        self.shell_limits_choice = shell_limits_choice
        self.neighborhood_size_as_fraction = neighborhood_size_as_fraction
        self.cut_off_position = cut_off_position
        self.threshold_difference_as_fraction = \
            threshold_difference_as_fraction
        self.maximum_index = maximum_index

        self.set_nonspecified_attributes_to_default(ARGUMENTS_DEFAULT)

        psi_n, shell_limits = self.determine_shell_limits_and_get_psi_n_all(from_computed,
                                                                            return_shell_limits=True)
        psi_abs = np.absolute(psi_n)

        gridscore_in_parts = [
            np.mean(
                psi_abs[self._idx_in_part_of_arena(
                    part='x_range', x_range=[i, i + 0.5])])
            for i in [0, 0.5]
        ]
        n = np.array([0, 1])
        width = 0.5
        left = n + 1 - width / 2.
        plt.bar(left, gridscore_in_parts, width=width,
                color=color_cycle_blue3[0])
        ax = plt.gca()
        plt.setp(ax, xticks=[1, 2],
                 xticklabels=['left', 'right'],
                 xlabel='Box side',
                 ylabel=r'$\Psi$ score')
        plt.locator_params(axis='y', tight=True, nbins=2)
        simpleaxis(ax)

    def _idx_in_part_of_arena(self, part='right_half', x_range=None):
        if part == 'right_half':
            separation = ((self.arena_limits[0, 0]
                           + self.arena_limits[0, 1]) / 2.)
            idx = np.argwhere(self.spikes.pos[:, 0] > separation)
        elif part == 'left_half':
            separation = ((self.arena_limits[0, 0]
                           + self.arena_limits[0, 1]) / 2.)
            idx = np.argwhere(self.spikes.pos[:, 0] <= separation)
        elif part == 'x_range':
            idx = np.argwhere(
                np.logical_and(
                    x_range[0] <= self.spikes.pos[:, 0],
                    self.spikes.pos[:, 0] <= x_range[1])
            )
        return idx

    def get_timewindows_of_trial_type(self, trial='baseline_start'):
        """
        Returns time windows for a trial type for Escobar 2016 data.

        One trial type can occur within several time windows, so more than
        one window might be returned.

        Parameters
        ----------
        trial : str {'baseline_start', 'baseline_end', 'l1', 'd1', ..., 'l4',
                    'd4'}
            The trial type

        Returns
        -------
        window : ndarray of shape (N, 2)
            If the trial type occurs only once (like 'baseline_start'):
            [[tstart0, tend0]]
            If the trial type occurs more than once (like 'l1'):
            [[tstart0, tend0], [tstart1, tend1], ...]
        """
        baselines = self.computed['baselines']
        trials = self.computed['trials']
        if trial == 'baseline_start':
            windows = baselines[0, 1:]
        elif trial == 'baseline_end':
            windows = self.computed['baselines'][1, 1:]
        else:
            i = str_to_int_escobar[trial]
            if i not in trials[:, 0]:
                print('Trial {0} is not part of this experiment'.format(
                    trial
                ))
                raise ValueError
            windows = trials[trials[:, 0] == i][:, 1:]
        return np.atleast_2d(windows)

    def concatenate_timewindows(self, trials=None):
        """
        Concatenate time intevals of multiple trials

        Parameters
        ----------
        trials : list of trial strings
            E.g. ['l1', 'd1']

        Returns
        -------
        a : ndarray of shape (N, 2)
            Note that this leads to time intervals that do not go from
            earlier times to later times. For example, if dark and light
            trials are interleaved So time intervals later in the list might
            correspond to earlier times.
            E.g.: Say trials = ['l1', 'd1] and
            [[tstart_l1_0, tend_l1_0], [tstart_l1_1, tend_l1_1],
                    [tstart_d1_0, tend_d1_0, [start_d1_1, tend_d1_1]]
        """
        a = np.empty((0, 2), np.float64)
        for trial in trials:
            a = np.concatenate((a, self.get_timewindows_of_trial_type(trial)))
        return a

    def get_timewindows_from_mode(self, mode):
        """Time windows in which grid scores were computed for the mode"""
        # If mode is None or it contains 'in_', the grid score was computed
        # for all spikes, so no time windows are needed.
        if mode is None or 'in_' in mode:
            return None
        else:
            try:
                trials = mode_arguments[mode]['trials']
                t_ints = self.concatenate_timewindows(trials=trials)
                return t_ints
            except KeyError:
                return None

    def spike_idx(self, n):
        """
        Index array into spike positions and spike times

        NB: This index arrays does not necessarily follow the temporal order
        of spikes.

        Parameters
        ----------
        n : {int, ndarray}
            If `n` is an integer, the first `n` spikes will be indexed.
            If `n` is a one dimensionals array, it is considered
            to be an index array already.
            If `n` is a two dimensioanl array, the rows contain time intervals.

        Returns
        -------
        idx : ndarray
            Index array into spike positions
        """
        if n is None:
            idx = np.arange(len(self.spikepositions))
        elif isinstance(n, int):
            # First n spikes
            idx = np.arange(n)
        elif isinstance(n, float):
            raise TypeError('n must be of type float or ndarray')
        else:
            dimensions = n.ndim
            if dimensions == 1:
                # n is already an index array
                idx = n
            elif dimensions == 2:
                # n contains time intervals
                # spiketimes must exist
                if self.spiketimes is None:
                    raise ValueError(
                        '''
                        Spiketimes is None, but spiketimes must be given, if n
                        contains time intervals
                        '''
                    )
                else:
                    st = self.spiketimes
                # Add spike indeces within all given intervals
                idx = np.array([], dtype=np.int64)
                for interval in n:
                    idx_in_interval = np.argwhere(
                        (interval[0] <= st) & (st < interval[1])
                    ).flatten()
                    idx = np.concatenate((idx, idx_in_interval))
        return idx

    def is_escobar_trial(self, spike_selection):
        """Check if spike selection is escobar trial"""
        if spike_selection is None:
            return False
        else:
            for s in spike_selection:
                if s in ['l1', 'l2', 'l3', 'l4', 'd1', 'd2', 'd3', 'd4',
                         'baseline_start', 'baseline_end']:
                    return True

    def spikemaps_in_trials(self, trials=('l1', 'l2', 'l3', 'l4')):
        gs = gridspec.GridSpec(2, 2)
        for n, trial in enumerate(trials):
            plt.subplot(gs[n])
            self.spikemap(spike_selection=[trial],
                          color_code=None, show_shell=False,
                          cut_off_position='arena_fraction',
                          maximum_index=0, dotsize=6,
                          from_computed=False)
            plt.title(trial)
    def spikemap(self,
                 shell_limits_choice='automatic_single',
                 color_code='psi_abs',
                 neighborhood_size_as_fraction=0.1,
                 cut_off_position=0,
                 threshold_difference_as_fraction=0.05,
                 noise=0,
                 noise_type='gaussian',
                 dotsize=7,
                 from_computed=False,
                 gridscore_norm=None,
                 colorbar_range='automatic',
                 std_threshold=None,
                 spike_selection=None,
                 show_shell=False,
                 n_symmetry=6,
                 compare_to_other_symmetries=True,
                 psi_title=True,
                 bins=200,
                 weights=None,
                 colorbar_label=True,
                 maximum_index=1,
                 show_colorbar=True,
                 axis=None,
                 shuffle_number=None,
                 show_isgrid=False,
                 show_title=True,
                 show_separating_wall=False,
                 ):
        # plt.style.use('dark_background')
        self.set_spikepositions(shuffle_number)
        self.spike_selection = spike_selection
        self.shell_limits_choice = shell_limits_choice
        self.neighborhood_size_as_fraction = neighborhood_size_as_fraction
        self.cut_off_position = cut_off_position
        self.threshold_difference_as_fraction = \
            threshold_difference_as_fraction
        self.gridscore_norm = gridscore_norm
        self.std_threshold = std_threshold
        self.n_symmetry = n_symmetry
        self.compare_to_other_symmetries = compare_to_other_symmetries
        self.bins = bins
        self.weights = weights
        self.maximum_index = maximum_index
        self.axis = axis
        # shell_limits = None
        # psi_n = self.computed['gridscores']['in_l2'][idx]
        self.spikes = self.get_spikes_class_for_spike_selection(spike_selection)

        print('Number of spikes: ', len(self.spikes.pos[:, 0]))
        self.add_noise(noise, type=noise_type)
        if color_code is not None:
            psi_n, shell_limits = self.determine_shell_limits_and_get_psi_n_all(
                from_computed, return_shell_limits=True)
        else:
            psi_n = np.ones(self.spikes.n_spikes)
            shell_limits = np.array([0, 1])

        print('shell limits: ', shell_limits)

        psi_abs_sorted, psi_arg_sorted, spikehd_sorted, x_sorted, y_sorted = \
            self._get_sorted_psiabs_psiarg_spikeheaddirections_x_y(psi_n)

        if color_code == 'psi_abs':
            c = psi_abs_sorted
        elif color_code == 'psi_arg':
            c = psi_arg_sorted
            colorbar_range = np.array([-180, 180]) / n_symmetry
        elif color_code == 'headdirection':
            c = np.rad2deg(spikehd_sorted)
            colorbar_range = np.array([-180, 180])
        else:
            # Every spike white
            # c = -np.ones_like(psi_abs_sorted)
            # # Every spike black
            c = np.ones_like(psi_abs_sorted)
            color_code = 'none'

        max_factor = 0.5 if gridscore_norm == 'all_neighbor_pairs' else 1
        # cmasked = np.ma.masked_where(c == 0, c)
        cnorm, cmap = self.get_colornorm_colormap(c, color_code,
                                                  max_factor=max_factor,
                                                  colorbar_range=colorbar_range)
        print('Nans', np.sum(np.isnan(c)))
        # extreme_color='white'
        extreme_color='black'
        cmap.set_over(extreme_color)
        cmap.set_under(extreme_color)
        cmap.set_bad(extreme_color)
        plt.scatter(
            x_sorted,
            y_sorted,
            c=c,
            s=dotsize, linewidths=0., edgecolors='none',
            norm=cnorm, cmap=cmap, alpha=1.0)
        plt.gca().set_aspect('equal')

        mean, std = self._get_mean_std_over_spikes(c, color_code)
        cb_ticks, cb_format, cb_label, cb_label_orientation, labelpad = \
            self._get_cb__ticks_format_label_orientation_pad(
                c, color_code, colorbar_range
            )
        if shell_limits_choice == 'automatic_single_for_quadratic':
            cb_label = r'$\hat{\psi}_k^{(4)}}$'
        if shell_limits_choice == 'automatic_single_for_bands':
            cb_label = r'$\hat{\psi}_k^{(2)}}$'
        if show_colorbar:
            cb = plt.colorbar(format=cb_format, ticks=cb_ticks)
            # plt.clim(cb_ticks[0], cb_ticks[1])
            if colorbar_label:
                cb.set_label(cb_label, rotation=cb_label_orientation,
                                  labelpad=labelpad)

        title = self._get_spikemap_title(color_code, psi_title, mean, std)

        if show_shell:
            origin = (x_sorted[-1], y_sorted[-1])
            self.show_shell_limits(shell_limits,
                                   origin=origin)

        if show_isgrid:
            isg = self.isgrid(method='psi_mean')
            s = u'\u2714' if isg else u'\u2718'
            # u'\u2717'
            # title += ', {0}'.format(isg)
            title += '  ' + s
        ax = plt.gca()
        plt.setp(ax,
                 xlim=self.arena_limits[0],
                 ylim=self.arena_limits[1],
                 xticks=[],
                 yticks=[])
        if show_title:
            plt.title(title, fontsize=12)

        if show_separating_wall:
            plt.axhline(y=0, color='gray', linestyle='dashed')
        # Revert all modifications to the spikes class
        self.spikes = self.get_spikes_class_for_spike_selection()

    def isgrid(self, method='psi_mean'):
        """
        Return True if cell is classified as grid, False otherwise

        NB: The percentiles and the gridscores need to be computed beforehand

        Parameters
        ----------
        method : str, optional {'psi_mean', 'langston', ...}

        Returns
        -------
        ret : bool
        """
        percentile = self.computed['gridscore_percentiles'][method]
        score = self.computed['gridscores'][method]
        return (score > percentile)

    def get_mode_from_parameters(self):
        """Returns mode key to computed grid score dictionaries"""
        mode = 'each_spike_final'
        if self.n_symmetry == 5 or self.n_symmetry == 7:
            mode += '_n_{0}'.format(self.n_symmetry)
        if not self.compare_to_other_symmetries:
            mode += '_no_cosy'
        if self.spike_selection == 'velocity_above_15':
            mode += '_velocity_15'
        if self.shell_limits_choice == '130':
            mode += '_shell_130'
        if self.axis:
            mode += '_axis_{0}'.format(self.axis)
        return mode

    def get_colornorm_colormap(self, c, color_code='psi_abs', max_factor=1,
                               colorbar_range='automatic'):
        if colorbar_range is 'automatic':
            if color_code == 'psi_abs':
                colornorm = mpl.colors.Normalize(0, np.amax(c) * max_factor)
            elif color_code == 'psi_arg':
                colornorm = mpl.colors.Normalize(-30, 30)
            elif color_code == 'headdirection':
                print(color_code)
                colornorm = mpl.colors.Normalize(-180, 180)
            else:
                colornorm = mpl.colors.Normalize(0, 1)
        else:
            colornorm = mpl.colors.Normalize(colorbar_range[0],
                                             colorbar_range[1])
        if color_code == 'psi_abs':
            cmap = getattr(mpl.cm, 'viridis')
        elif color_code == 'psi_arg':
            cmap = getattr(mpl.cm, 'hsv')
        elif color_code == 'headdirection':
            # import seaborn as sns
            # from matplotlib.colors import ListedColormap
            # cmap_from_seaborn = sns.color_palette("husl", 360)
            # cmap = ListedColormap(cmap_from_seaborn)
            cmap = getattr(mpl.cm, 'hsv')
        else:
            # Use greys to eithe get all white (-1, for cover) or all black (
            # +1 for figures)
            cmap = getattr(mpl.cm, 'Greys')
        return colornorm, cmap

    def _get_mean_std_over_spikes(self, values, color_code):
        if color_code == 'psi_arg':
            high = 30
            low = -30
            c_without_zeros = values[values != 0][~np.isnan(values)]
            mean = stats.circmean(c_without_zeros, high=high, low=low)
            std = stats.circstd(c_without_zeros, high=high, low=low)
        elif color_code == 'headdirection':
            high = 180
            low = -180
            c_without_zeros = values[values != 0]
            mean = stats.circmean(c_without_zeros, high=high, low=low)
            std = stats.circstd(c_without_zeros, high=high, low=low)
        else:
            mean = np.nanmean(values)
            std = np.nanstd(values)
        return mean, std

    def _get_sorted_psiabs_psiarg_spikeheaddirections_x_y(self, psi_n):
        psi_abs = np.absolute(psi_n)
        # Each angle between two neighboring spikes is increased in psi
        # by a factor of n. To get the local orientation, we need to
        # undo this rotation by dividing by n_symmetry.
        # NB: np.angle returns angles in the range -Pi to +Pi.
        # So for n_symmetry = 6 the line below  leads to
        # angles between -30 and 30 degrees.
        psi_arg = (np.angle(psi_n, deg=True)) / self.n_symmetry
        # Sort everthing according to psi_abs values
        # Then better values are more visible
        sort_idx = np.argsort(psi_abs)
        # good_idx = sort_idx[int(np.floor(len(sort_idx) / 5)):]
        # idx = idx[good_idx]
        psi_abs = psi_abs[sort_idx]
        psi_arg = psi_arg[sort_idx]
        try:
            spikehd = self.spikeheaddirections[sort_idx]
        except TypeError:
            spikehd = np.zeros_like(psi_abs)
        x = self.spikes.pos[:, 0]
        y = self.spikes.pos[:, 1]
        x = x[sort_idx]
        y = y[sort_idx]
        return psi_abs, psi_arg, spikehd, x, y

    def _get_cb__ticks_format_label_orientation_pad(self,
        c, color_code, colorbar_range):
        if color_code == 'psi_arg':
            cb_ticks = np.array([-30, 0, 30])
            cb_format = '%d'
            cb_label = orientation_str
            cb_label_orientation = 'vertical'
            labelpad = -2
        elif color_code == 'headdirection':
            cb_ticks = np.array([-180, 0, 180])
            cb_format = '%d'
            cb_label = 'Head direction'
            cb_label_orientation = 'vertical'
            labelpad = -4
        else:
            if colorbar_range == 'automatic':
                maximum = np.amax(c)
                if maximum == 0:
                    maximum = 0.1
                cb_ticks = np.array([0, maximum])
            else:
                cb_ticks = np.array([colorbar_range[0], colorbar_range[1]])
            cb_format = '%.1f'
            cb_label = gridscore_str
            cb_label_orientation = 'vertical'
            labelpad = -15
        return cb_ticks, cb_format, cb_label, cb_label_orientation, labelpad

    def _get_spikemap_title(self, color_code, psi_title, mean, std):
        norm_dict = {None: 'none', 'all_neighbor_pairs': 'alln'}
        th = 0. if self.std_threshold is None else self.std_threshold
        title = 'M:{0:.2f} SD:{1:.2f}T:{3:.2f}n:{4}N:{2}'.format(
            mean, std, norm_dict[self.gridscore_norm], th, self.n_symmetry)
        if psi_title:
            if color_code == 'psi_abs':
                if self.shell_limits_choice == 'automatic_single_for_bands':
                    title_str = r'$\Psi^{{(2)}}$ = {0:.2f}'
                elif self.shell_limits_choice == \
                        'automatic_single_for_quadratic':
                    title_str = r'$\Psi^{{(4)}}$ = {0:.2f}'
                else:
                    # title_str = mean_gridscore_str + ' = {0:.2f}'
                    title_str = '{0:.2f}'
                title = title_str.format(mean)
            elif color_code == 'psi_arg':
                if np.isnan(mean):
                    title = mean_orientation_str + r' = invalid'
                else:
                    # title = (mean_orientation_str
                    #          + r' = {0:d}$^\circ$').format(int(round(mean)))
                    title = (r'{0:d}$^\circ$').format(int(round(mean)))
            elif color_code == 'headdirection':
                title = '$\mu = {0:.0f}$, $\sigma = {1:.0f}$'.format(mean, std)
            else:
                title = '0.3'
        return title

    def show_shell_limits(self, shell_limits, origin=(0, 0)):
        n_shells = self.spikes.get_number_of_shells(shell_limits)
        plt.plot([origin[0]], [origin[1]], marker='x', color='black')
        if n_shells == 0:
            pass
        elif n_shells == 1:
            for r, c in [(shell_limits[0], 'red'),
                         (shell_limits[1], 'red')]:
                circle = plt.Circle(origin,
                                    r, ec=c, fc='none', lw=2,
                                    linestyle='dashed')
                plt.gca().add_artist(circle)
        elif n_shells == 2:
            for r, c in [(shell_limits[0, 0], 'red'),
                         (shell_limits[0, 1], 'red')]:
                circle = plt.Circle((0, 0),
                                    r, ec=c, fc='none', lw=2,
                                    linestyle='dashed')
                plt.gca().add_artist(circle)
            for r, c in [(shell_limits[1, 0], 'black'),
                         (shell_limits[1, 1], 'black')]:
                circle = plt.Circle((0, 0),
                                    r, ec=c, fc='none', lw=2,
                                    linestyle='dashed')
                plt.gca().add_artist(circle)

    def ratemap(self, sigma=5, spike_selection=None, show_colorbar=True):
        # if self.is_escobar_trial(spike_selection):
        #     n = self.concatenate_timewindows(
        #         trials=np.atleast_1d(spike_selection))
        # else:
        #     n = spike_selection
        # idx = self.spike_idx(n=n)
        self.spikes = self.get_spikes_class_for_spike_selection(spike_selection)
        rm = self.spikes.get_ratemap(sigma=sigma)
        np.save('test_ratemap.npy', rm)
        plt.imshow(rm.T, origin='lower left', cmap=self.cmap)
        plt.setp(plt.gca(),
        		 xticks=[], yticks=[])
        if show_colorbar:
            plt.colorbar()

    def heatmap(self, bins=100):
        hm = self.spikes.get_heatmap(bins=bins)
        plt.imshow(hm.T, origin='lower left', cmap=self.cmap)
        plt.colorbar()

    def distance_histogram(self, neighborhood_size_as_fraction=0.1,
                           cut_off_position=0,
                           threshold_difference_as_fraction=0.05, noise=0,
                           noise_type='gaussian', spike_selection=None,
                           bins=200,
                           weights=None, maximum_index=1,
                           custom_vline=None,
                           show_maxima=True):
        self.spikes = self.get_spikes_class_for_spike_selection(spike_selection)
        self.add_noise(noise, type=noise_type)
        n, centers = self.spikes.get_distancehistogram_and_centers(bins=bins,
                                                                   weights=weights)
        self._plot_distance_histogram(n, centers,
                                      neighborhood_size_as_fraction
                                      =neighborhood_size_as_fraction,
                                      cut_off_position=cut_off_position,
                                      threshold_difference_as_fraction
                                      =threshold_difference_as_fraction,
                                      bins=bins,
                                      weights=weights,
                                      maximum_index=maximum_index,
                                      custom_vline=custom_vline,
                                      show_maxima=show_maxima
                                      )

    def _plot_distance_histogram(self, n,
                                 centers, neighborhood_size_as_fraction=0.1,
                                 cut_off_position=0,
                                 threshold_difference_as_fraction=0.05,
                                 bins=200,
                                 weights=None, maximum_index=1,
                                 custom_vline=None,
                                 show_maxima=True):
        # width = centers[1] - centers[0]
        # plt.bar(centers, n, align='center', width=width, color='black',
        #         alpha=0.2)
        plt.plot(centers, n, color='black')
        if show_maxima:
            self.plot_maxima(n, centers, threshold_difference_as_fraction,
                             neighborhood_size_as_fraction)
        shell_limits = self.spikes.get_shell_limits(
            shell_limits_choice='automatic_single',
            neighborhood_size_as_fraction=neighborhood_size_as_fraction,
            cut_off_position=cut_off_position,
            threshold_difference_as_fraction=threshold_difference_as_fraction,
            bins=bins, weights=weights, maximum_index=maximum_index)
        self.indicate_typical_distance(shell_limits, custom_vline)
        ax = plt.gca()
        plt.setp(ax,
                 xlabel='Distance',
                 ylabel='# Pairs',
                 # xticks=[0, 1],
                 # yticks=ax.get_yticks()/1000
                 )
        ax.locator_params(axis='y', tight=True, nbins=3)
        ax.locator_params(axis='x', tight=True, nbins=5)
        simpleaxis(ax)
        if not np.any(np.isnan(shell_limits)):
            extraticks = [np.mean(shell_limits)]
            plt.xticks(list(plt.xticks()[0]) + extraticks)
        plt.margins(0.1)

    def indicate_typical_distance(self, shell_limits, custom_vline=None):
        typical_distance = np.mean(shell_limits)
        if typical_distance is not None:
            # plt.axvline(x=typical_distance, lw=2, color='gray')
            plt.axvline(x=5/6. * typical_distance,
                        lw=1, color=color_cycle_red3[1], linestyle='--')
            plt.axvline(x=7/6. * typical_distance,
                        lw=1, color=color_cycle_red3[1], linestyle='--')
            if custom_vline is not None:
                plt.axvline(x=custom_vline,
                            lw=2, color='black', linestyle='dotted')
        print('typical distance: ', typical_distance)

    def plot_maxima(self, n, centers, threshold_diff, neighborhood_size):
        maxima_positions, maxima_values = \
            get_maximapositions_maximavalues(
                x=centers, y=n,
                threshold_difference_as_fraction
                =threshold_diff,
                neighborhood_size_as_fraction=neighborhood_size)
        plt.plot(maxima_positions, maxima_values,
                 marker='o', color='gray', linestyle='none',
                 markersize=5)

    def add_noise(self, noise, type='gaussian'):
        if noise != 0:
            s_x, s_y = self.spikes.pos.shape
            if type == 'gaussian':
                pos_noise = noise * np.random.randn(s_x, s_y)
            elif type == 'uniform':
                pos_noise = (
                        2 * noise * np.random.random_sample((s_x, s_y)) -
                        noise)
            self.spikes.pos += pos_noise
        else:
            pass

    def grid_score_vs_noise(self, norm=None):
        gs_sargolini = np.array([
            0.91, 0.91, 0.85, 0.78, 0.46, -0.029,
            0.39, -0.086, -0.36, -0.60, -2.0, -1.1
        ])
        gs_psi = np.array([
            0.47, 0.471, 0.467, 0.46, 0.441, 0.402,
            0.42, 0.344, 0.278, 0.201, 0.0642, 0.0487
        ])
        gs_psi_no_shell = np.array([
            0.11, 0.11, 0.108, 0.104, 0.101, 0.0951,
            0.101, 0.0881, 0.0769, 0.0691, 0.061, 0.0608
        ])
        my_slice = slice(0, 12)
        noise = np.arange(12)
        plt.subplot(311)
        gs = self.get_normalized_gs(gs_sargolini, norm=norm)
        plt.plot(noise[my_slice], gs, color='red')
        plt.ylabel('GS Sargolini')
        # plt.ylim([-2, 2])
        plt.subplot(312)
        gs = self.get_normalized_gs(gs_psi, norm=norm)
        plt.plot(noise[my_slice], gs, color='blue')
        # plt.ylim([0, 1])
        plt.xlabel('Uniform noise')
        plt.ylabel('GS psi')
        plt.subplot(313)
        gs = self.get_normalized_gs(gs_psi_no_shell, norm=norm)
        plt.plot(noise[my_slice], gs, color='blue')
        # plt.ylim([0, 1])
        plt.xlabel('Uniform noise')
        plt.ylabel('GS psi, no shell')

    def get_normalized_gs(self, grid_scores, norm=None):
        """
        Return normalized gs

        Parameters
        ----------
        grid_score : (N,) ndarray
            Array with N grid scores
        norm : str
            None :
            'start_one': Normalizes it such that the start value is 1

        Returns
        -------
        normalized_gs : (N,) ndarray
        """
        if norm == 'start_one':
            normalized_gs = grid_scores / grid_scores[0]
        elif norm is None:
            normalized_gs = grid_scores
        return normalized_gs

    def correlogram(self, method=None, sigma=5, noise=0,
                    noise_type='gaussian', n_spikes=None,
                    show_doughnut=True, mode='full',
                    shuffle_number=None, show_isgrid=False):
        """
        Plots a correlogram

        Parameters
        ----------
        n_spikes : int, optional
            See get_spikes_class_for_spike_selection for documenttion

        Returns
        -------
        """
        self.spikes = self.get_spikes_class_for_spike_selection(
            spike_selection=n_spikes)
        self.set_spikepositions(shuffle_number=shuffle_number)
        self.add_noise(noise, type=noise_type)
        # rm = self.spikes.get_list_of_ratemaps(sigma=sigma)
        rm = self.spikes.get_ratemap(sigma=sigma)
        a = pearson_correlate2d(rm, rm, mode=mode)
        corr_spacing = a.shape[0]
        # np.save('/Users/simonweber/programming/workspace/gridscore
        # /example_correlogram', a)
        # plt.imshow(a.T, vmin=-1, vmax=1, origin='lower left', cmap=self.cmap)
        corr_limits = 1 if mode == 'same' else 2
        corr_linspace = corr_limits * np.linspace(
            self.arena_limits[0, 0],
            self.arena_limits[0, 1],
            corr_spacing)
        X, Y = np.meshgrid(corr_linspace, corr_linspace)
        V = np.linspace(-1.0, 1.0, 30)
        ax = plt.gca()
        ax.set_aspect('equal')
        plt.contourf(X, Y, a.T, V, cmap=self.cmap)
        plt.colorbar(format='%.0f', ticks=np.array([-1, 1]))
        if method is not None:
            radius = (
                             self.spikes.arena_limits[0, 1]
                             - self.spikes.arena_limits[0, 0]) / 2.
            if mode == 'full':
                correlogram_radius = 2 * radius
            else:
                correlogram_radius = radius
            gridness = Gridness(
                a, radius=correlogram_radius, method=method,
                neighborhood_size=10, threshold_difference=0.2,
                n_contiguous=100)
            grid_score = gridness.get_grid_score()

            if show_doughnut:
                title = 'GS: {0:.2}'.format(grid_score)
                for r, c in [(gridness.inner_radius, 'black'),
                             (gridness.outer_radius, 'black'),
                             (gridness.grid_spacing, 'white')]:
                    circle = plt.Circle((0, 0),
                                        r, ec=c, fc='none', lw=2,
                                        linestyle='dashed')
                    plt.gca().add_artist(circle)
            else:
                title = '{0:.1f}'.format(grid_score)

            if show_isgrid:
                isg = self.isgrid(method='langston')
                s = u'\u2714' if isg else u'\u2718'
                title += '  ' + s
            plt.title(title, fontsize=12)
        plt.xticks([])
        plt.yticks([])

    def dummy(self):
        plt.plot(np.arange(5))

    # pass

    def gridscore_distribution(self,
                               shell_limits_choice=None,
                               neighborhood_size_in_percent=0.1,
                               cut_off_position=0,
                               threshold_difference_in_percent=0.05,
                               from_computed=False,
                               gridscore_norm=None,
                               std_threshold=None,
                               n_spikes=None):
        # Reset the spikes class such that it only considers the first n spikes
        # self.spikes = self.get_spikes_class_for_spike_selection(
        # 	n_spikes=n_spikes)

        shell_limits = self.spikes.get_shell_limits(
            shell_limits_choice=shell_limits_choice,
            neighborhood_size_as_fraction=neighborhood_size_in_percent,
            cut_off_position=cut_off_position,
            threshold_difference_as_fraction=threshold_difference_in_percent)
        psi_n = self.determine_shell_limits_and_get_psi_n_all(n_symmetry=6,
                                                              shell_limits_choice=shell_limits_choice,
                                                              from_computed=from_computed,
                                                              gridscore_norm=gridscore_norm,
                                                              std_threshold=std_threshold)
        psi_abs = np.absolute(psi_n)
        n, bins, patches = plt.hist(psi_abs, bins=20)
        plt.xlabel('Grid score')
        plt.ylabel('Frequency')
        plt.axvline(x=np.mean(psi_abs), color='black')
        plt.axvline(x=np.median(psi_abs), color='red')
        plt.axvline(x=bins[np.argmax(n)] + (bins[1] - bins[0]) / 2,
                    color='green')

    def headdirection_polar(self):
        """
        Plots histogram of trajectory head directions and spike head directions
        """
        # 30 degrees for each bin
        bins = 12
        # All headdirections
        headdirections = self.headdirections
        # Head directions at spike firing
        spikeheaddirections = self.spikeheaddirections
        hd_tuning = Head_Direction_Tuning(HD_firing_rates=None,
                                          spacing=None)
        plt.hist(spikeheaddirections,
                 bins=12,
                 color=color_cycle_blue3[1],
                 normed=True)
        # Plot head direction tuning of all positions in circle
        plt.hist(headdirections,
                 bins=bins,
                    color='black',
                    normed=True,
                    facecolor='none',
                    edgecolor='black',
                    zorder=100)
        # Watson U2
        alpha = 0.01
        watson_u2, passed = hd_tuning.watson_u2(headdirections,
                                                spikeheaddirections,
                                                alpha=alpha)
        print('watson U2', watson_u2, passed)
        if not passed:
            watson_u2 = np.nan
        # Vector based head direction tuning
        vector_hd = self.vector_head_direction_tuning(spikeheaddirections)
        n_spikes = len(spikeheaddirections)
        text_str = self._get_hd_tuning_title(n_spikes, vector_hd, watson_u2)
        plt.title(text_str)
        ttl = plt.gca().title
        ttl.set_position([.5, 1.00])
        plt.xticks([])
        plt.yticks([])

    @staticmethod
    def _get_hd_tuning_title(n_spikes, vector_hd, watson_u2):
        """Title of HD tuning histograms"""
        # For U2 values larger than 10, we want them to be integers
        # This avoids an automatic switch to exponential notation, if we use
        # precision 2.
        if watson_u2 >= 10:
            watson_u2 = int(watson_u2)
            # Decimal integer
            format_str = 'd'
        else:
            # Float with precision 2
            format_str = '.2g'

        # NB: To modify the format string, you need to escape the curly
        # braces, otherwise everything within them would be replaced.
        raw_str = (r'$N = {n_spikes}$, $v = {vector_hd:.2}$, '
                   + r'$U^2 = {{watson_u2:{0}}}$'.format(format_str))
        print(raw_str)
        text_str = raw_str.format(
            n_spikes=n_spikes,
            vector_hd=vector_hd,
            watson_u2=watson_u2)
        return text_str

    @staticmethod
    def vector_head_direction_tuning(angles):
        """
        Sums up vectors and normalizes with number of vectors

        Parameters
        ----------
        anlges : ndarray
            Angles of the vectors

        Returns
        -------
        ret : float
            The vector based head direction tuning
        """
        vectors = np.array([
            np.cos(angles),
            np.sin(angles)
        ])
        s = np.sum(vectors, axis=1)
        return np.linalg.norm(s) / vectors.shape[1]

    @staticmethod
    def get_timewindows(n, last_spiketime, windowsize=None,
                        windowmethod='fixed_size'):
        """
        Returns timewindows for running averages in compute or for plotting

        NB: In `compute`, `gridscores_within_timewindows` computes the grid
        score for only the spikes within a time window
        In `plotting`, given grid scores (for spikes within whatever time
        window) we show the means within time intervals.

        Parameters
        ----------
        n : int
            Number of windows returned
        windowsize : float
            The width of the timewindow in seconds
        last_spiketime : float
            The time of the last spike
        windowmethod : str {'fixed_size', 'decrease_from_left'}
            How to choose the windows.
            For 'fixed_size' a window of the size given by *windowsize*
            is moved from left to right in *n* steps.
            For 'decrease_from_left', the window initially contains all
            spikes and is decreased from the left in *n* steps, until only
            the last spike remains.
            For 'incrase' the time window is increased from the left. Note,
            that the inital time window is not [0, 0], because this would
            be useless, but [0, last_spiketime/n] instead.

        Returns
        -------
        timewindows : ndarray of shape (n, 2)
            Array of n start and end times
        """
        if windowmethod is 'fixed_size':
            twindow_starts = np.linspace(0,
                                         last_spiketime - windowsize,
                                         n)
            twindow_ends = twindow_starts + windowsize
        elif windowmethod is 'decrease_from_left':
            twindow_starts = np.linspace(0, last_spiketime, n)
            twindow_ends = np.repeat(last_spiketime, n)
        elif windowmethod is 'increase':
            twindow_starts = np.repeat(0, n)
            twindow_ends = np.linspace(last_spiketime / n, last_spiketime, n)
        elif windowmethod is 'bins':
            binsize = last_spiketime / n
            twindow_starts = np.arange(n) * binsize
            twindow_ends = twindow_starts + binsize
        timewindows = np.dstack((twindow_starts, twindow_ends))[0]
        return timewindows

    @staticmethod
    def shift_timewindows_to_relative_timewindows(timewindows):
        """
        Subtract the start time from each time window

        Both from the start time and the end time (this is ensures by adding
        a new axis)
        """
        relative_timewindows = timewindows  - timewindows[:, 0, np.newaxis]
        return relative_timewindows

    def _get_computed_windows_and_gridscores(self,
            name='mean_gridscore_in_sliding_windows',
            trial=('l2', 'd2'),
            mode='in_l2d2',
            size=60,
            dt=5):
        """
        Returns timewindows and gridscore stats within each time window

        Parameters
        ----------
        name : str
            Specifies the windowing method and the stats
        trial : str or tuple of str
            E.g. 'l2' or ('l2', 'd2')
            Tuples are only valid for a windowing method that uses light and
            dark trials together (like sliding windows)
        mode : str
        size : float, optional
            Only necessary for sliding windows
        dt : float, optional
            Only necessary for sliding windows

        Returns
        -------
        c : ndarray of shape (N, 3)
            Each line is a time window.
            Column 0: start of time window
            Column 1: end of time windows
            Column 2: single grid score value in the time window
        """
        self.set_params_rawdata_computed(psp=self.psps[0])
        if name in ['mean_gridscore_in_trial_intervals',
                    'mean_gridscore_in_time_blocks']:
            c = self.computed[name][trial][mode]
        elif name in ['mean_gridscore_in_sliding_windows']:
            trial_string = ''.join(trial)
            c = self.computed[name][trial_string][mode][str(size)][str(dt)]
        return c

    def gridscore_vs_light_and_dark(self,
            name='mean_gridscore_in_sliding_windows',
            trial=('l2', 'd2'),
            mode='in_l2d2',
            size=60,
            dt=5):
        """
        Boxplot gridscore in light and in dark.

        Parameters
        ----------
        kwargs : dict
            See _get_computed_windows_and_gridscores()
            Trial nees to be a light trial
        """
        windows_and_gridscores = self._get_computed_windows_and_gridscores(
            name, trial, mode, size, dt
        )
        t_center = intervals2centers(windows_and_gridscores[:, :2])
        trial_windows = self.get_timewindows_of_trial_type(trial[0])
        light_windows = self.create_light_windows(
            len(trial_windows), size=120)
        is_light = self.is_light_time(t_center, light_windows)
        gridscores = windows_and_gridscores[:, 2]
        gridscores_in_light = gridscores[is_light]
        gridscores_in_dark = gridscores[~is_light]
        plt.boxplot([gridscores_in_light, gridscores_in_dark])

    def is_light_time(self, times, light_windows):
        """
        Checks if time is in light window.

        Parameters
        ----------
        times : ndarray of shape (N)
            Times.
        light_windows : ndarray of shape (K, 2)
            Windows of light time

        Returns
        is_light : ndarray of shape (N)
            Boolian array, True where time was in light.
        -------
        """
        is_light = np.zeros_like(times)
        for tw in light_windows:
            condition = np.logical_and(
                tw[0] <= times, times < tw[1])
            is_light[condition] = 1
        return is_light.astype(np.bool)

    def create_light_windows(self, n, size=120):
        """
        Creates windows of light times in a contiguous way

        NB: The resulting windows are contiguous, of equal size, and assume
        that a light trial is always followed by a dark trial.
        It would be nicer to construct them automatically, but it is not easy
        to get the 'inverse' of 'make_contiguous'.

        Parameters
        ----------
        n : int
            The number of windows
        size : float
            The size (width) of the window
        """
        step =  2 * size
        starts = np.arange(0, n*step, step)
        ends = starts + size
        return np.hstack((starts[:, np.newaxis], ends[:, np.newaxis]))

    def print_filenames_of_sane_cells_escobar(self):
        """Print filenames of Escobar cells with less than N spikes"""
        n_cells_with_few_spikes = 0
        for psp in self.psps:
            self.set_params_rawdata_computed(psp, set_sim_params=False)
            if len(self.plotsnep.rawdata['spiketimes']) <= 5e4:
                print('\'' + psp[('dat', 'filename')].quantity + '\'' + ',')
                n_cells_with_few_spikes +=1
        print('Sane cells:', n_cells_with_few_spikes)

    def print_filenames_and_light_id_of_grid_cells(self, min_gridscore=0.13):
        """Print trial and filename of groo grid cells"""
        light_ids = [1, 2, 3, 4]
        # light_ids = [3]
        n_good_grid_cells = 0
        for psp in self.psps:
            self.set_params_rawdata_computed(psp, set_sim_params=False)
            for lid in light_ids:
                trial = 'l{0}'.format(lid)
                mode = 'in_l{0}'.format(lid)
                try:
                    mean_gs = np.mean(self.plotsnep.computed[
                                          'mean_gridscore_in_trial_intervals'][
                                          trial][mode][:, 2])
                except KeyError:
                    continue
                # print(mean_gs)
                if mean_gs >= min_gridscore:
                    print(
                        '('
                        + str(lid)
                        + ','
                        + '\''
                        + psp[('dat', 'filename')].quantity
                        + '\''
                        + ')'
                        + ',')
                    n_good_grid_cells += 1
        print('Good grid cells:', n_good_grid_cells)

    def _set_default_attributes(self, **kwargs):
        self.shell_limits_choice = 'automatic_single'
        self.neighborhood_size_as_fraction = 0.1
        self.cut_off_position = 0
        self.threshold_difference_as_fraction = 0.05
        self.gridscore_norm = None
        self.std_threshold = None
        self.n_symmetry = 6
        self.compare_to_other_symmetries = True
        self.bins = 200
        self.weights = None
        self.maximum_index = 1
        self.axis = None


    def psi_abs_histogram(self, from_computed=False, cut_off_position=0,
                          maximum_index=1, spike_selection=None):
        # plt.style.use('dark_background')
        self._set_default_attributes()
        self.cut_off_position = cut_off_position
        self.maximum_index = maximum_index

        self.spikes = self.get_spikes_class_for_spike_selection(
            spike_selection)
        print('Number of spikes: ', len(self.spikes.pos[:, 0]))
        psi_n, shell_limits = self.determine_shell_limits_and_get_psi_n_all(
            from_computed, return_shell_limits=True)
        print('shell limits: ', shell_limits)
        psi_abs = np.absolute(psi_n)
        n, bins, patches = plt.hist(
            psi_abs, bins=np.linspace(0, 1, 21),
            histtype='step', color=color_cycle_blue3[1])
        max_of_good_gridscore_bin = np.amax(n[1:])
        ax = plt.gca()
        plt.setp(ax,
                 xlabel='Grid score', ylabel='Frequency',
                 ylim=[0, max_of_good_gridscore_bin * 1.1])
        plt.text(0.025, max_of_good_gridscore_bin, int(n[0]),
                 horizontalalignment='center')
        psi_abs_without_zeros_sorted = np.sort(psi_abs[psi_abs > 1e-6])
        skewness = stats.skew(psi_abs_without_zeros_sorted)
        pearson_2nd_skew = self.pearson_second_skewness(
            psi_abs_without_zeros_sorted)
        # plt.title('Skw = {0:.3}, P2 = {1:.3}'.format(
        #     skewness, pearson_2nd_skew))
        plt.title('')

    def pearson_second_skewness(self, a):
        return 3 * (np.mean(a) - np.median(a)) / np.std(a)

    def psi_arg_histogram(self, from_computed=False, cut_off_position=0,
                          maximum_index=1, spike_selection=None,
                          compare_to_other_symmetries=False):
        self._set_default_attributes()
        self.compare_to_other_symmetries = compare_to_other_symmetries
        self.cut_off_position = cut_off_position
        self.maximum_index = maximum_index

        self.spikes = self.get_spikes_class_for_spike_selection(spike_selection)
        print('Number of spikes: ', len(self.spikes.pos[:, 0]))
        psi_n, shell_limits = self.determine_shell_limits_and_get_psi_n_all(from_computed,
                                                                            return_shell_limits=True)
        print('shell limits: ', shell_limits)
        psi_arg = (np.angle(psi_n, deg=True)) / self.n_symmetry
        bins = np.linspace(-30, 30, 25)
        plt.hist(psi_arg, bins=bins)
        ax = plt.gca()
        plt.setp(ax,
                 xlabel='Orientation', ylabel='Frequency')

    def local_stats_map(self,
            from_computed=True,
            cut_off_position=0,
            maximum_index=1,
            spike_selection=None,
            compare_to_other_symmetries=True,
            nx=1, ny=1,
            colorbar_range='automatic',
            observable='psi_abs',
            n_symmetry=6,
            shell_limits_choice='automatic_single',
            partitioning='rectangular',
            return_stats=False,
            dotsize=7,
            show_spikes=False,
            axis=None
            ):
        self._set_default_attributes()
        self.compare_to_other_symmetries = compare_to_other_symmetries
        self.cut_off_position = cut_off_position
        self.maximum_index = maximum_index
        self.n_symmetry = n_symmetry
        self.spike_selection = spike_selection
        self.shell_limits_choice = shell_limits_choice
        self.axis = axis

        self.spikes = self.get_spikes_class_for_spike_selection(spike_selection)
        print('Number of spikes: ', len(self.spikes.pos[:, 0]))
        values, sttstcs = self._get_values_and_sttstcs(observable,
                                                       from_computed)
        xlim, ylim = self.arena_limits[0], self.arena_limits[1]
        if partitioning == 'triangular':
            from general_utils.arrays import paths_for_triangular_partitioning
            # y shift to increase triangle side
            y_s = 25
            x_s = y_s / np.tan(np.pi/6)

            paths = paths_for_triangular_partitioning(
                xlim=xlim + np.array([-x_s, x_s]),
                y_shift=ylim[0] - y_s,
                n=nx
            )
        elif partitioning == 'diagonal':
            paths = paths_for_diagonal_partitioning(xlim=xlim, ylim=ylim, n=nx)
        else:
            paths = paths_for_rectangular_partitioning(xlim=xlim,
                                                   ylim=ylim,
                                                   nx=nx, ny=ny)
        stats_in_paths = self.get_stats_in_paths(
            positions=self.spikes.pos, values=values, paths=paths,
            sttstcs=sttstcs)

        if return_stats:
            return stats_in_paths

        self.plot_stats_in_paths(observable, values, paths,
                                 stats_in_paths, colorbar_range)
        if show_spikes:
            plt.scatter(self.spikes.pos[:,0], self.spikes.pos[:,1],
                        s=dotsize, zorder=1000)


    def plot_stats_in_paths(self,
                            observable,
                            values,
                            paths,
                            stats_in_paths,
                            colorbar_range,
                            margin_factor=0):
        ax = plt.gca()
        ax.set_aspect('equal')
        norm, cmap = self.get_colornorm_colormap(
            values, color_code=observable, colorbar_range=colorbar_range)
        cmap.set_over('white')
        cmap.set_under('white')
        cmap.set_bad('white')

        for n, path in enumerate(paths):
            stats_in_path = stats_in_paths[n]
            print('stats in path', stats_in_path)
            facecolor = cmap(norm(stats_in_path))
            patch = patches.PathPatch(path, facecolor=facecolor, lw=1)
            ax.add_patch(patch)
        ax = plt.gca()
        mx = np.absolute(np.diff(self.arena_limits[0])[0]) * margin_factor
        margins_x = np.array([-mx, mx])
        plt.setp(ax,
                 xlim=self.arena_limits[0] + margins_x,
                 ylim=self.arena_limits[1] + margins_x,
                 xticks=[],
                 yticks=[])
        plt.margins(0.4)
        return norm, cmap

    def _get_values_and_sttstcs(self, observable='psi_abs',
                                from_computed=False):
        if observable == 'psi_arg':
            psi_n = self.determine_shell_limits_and_get_psi_n_all(from_computed)
            values = (np.angle(psi_n, deg=True)) / self.n_symmetry
            sttstcs = 'circmean'
        else:
            psi_n = self.determine_shell_limits_and_get_psi_n_all(from_computed)
            values = np.absolute(psi_n)
            sttstcs = 'mean'
        return values, sttstcs

    def get_stats_in_paths(self, positions, values, paths, sttstcs):
        """
        Returns stastics on values corresponding to positions with paths

        Parameters
        ----------
        positions : ndarray of shape (N, 2)
            Positions
        values : ndarray of shape (N)
            Value corresponding to each position
        paths : list of matplotlib paths of length M
            Path objects

        Returns
        -------
        stats_in_paths : ndarray of shape (M)
        """
        stats_in_paths = []
        for path in paths:
            contained_bool = path.contains_points(positions)
            values_in_path = values[contained_bool]
            stats_in_path = self._stats(values_in_path, sttstcs=sttstcs)
            stats_in_paths.append(stats_in_path)
        stats_in_paths = np.asarray(stats_in_paths)
        return stats_in_paths

    def velocity_vs_time(self, n=None):
        from general_utils.misc import velocities_from_positions

        slc = slice(0, n)
        velocities = velocities_from_positions(self.ratpositions,
                                               self.positiontimes)
        for n, kernel_size in enumerate([501]):
            plt.subplot(3, 1, n+1)
            v = signal.medfilt(velocities, kernel_size=kernel_size)
            plt.plot(self.positiontimes[:-1][slc], v[slc])
            plt.axhline(y=np.mean(v))
            plt.axhline(y=5, linestyle='dashed', color='black')
            plt.axhline(y=15, linestyle='dashed', color='black')
        ax = plt.gca()
        plt.setp(ax,
                 xlabel='Time [s]',
                 ylabel='Velocity [cm/s]')
        # timewindows = self.computed['timewindows_high_velocity']['3']
        # for tw in timewindows:
        #     plt.axvspan(xmin=tw[0], xmax=tw[1], alpha=0.5, color='black')

    def set_spikepositions(self, shuffle_number=None):
        if shuffle_number is None:
            pass
        else:
            self.spikepositions = self.rawdata[
                'spikepositions_shuffled'][str(shuffle_number)]

if __name__ == '__main__':
    # # Simple test data
    # angle0 = 0
    # angle1 = np.pi / 3
    # angle2 = 2 * angle1
    # angle3 = 3 * angle1
    # angle4 = 4 * angle1
    # angle5 = 5 * angle1
    # rho = 1
    # spikepositions = np.array(
    # 	[
    # 		[0, 0],
    # 		pol2cart(rho, angle0),
    # 		pol2cart(rho, angle1),
    # 		pol2cart(rho, angle2),
    # 		pol2cart(rho, angle3),
    # 		pol2cart(rho, angle4),
    # 		pol2cart(rho, angle5),
    # 	]
    # )
    # #----------------------------

    ###########################################################################
    ############################ Hafting 2005 data ############################
    ###########################################################################
    from .spikedata import SpikeData

    n = 4
    i = 1
    spikedata = SpikeData(
        filename='figure2d_trial1',
        identifier='rat11015_t1c1'
    )
    spikepositions = spikedata.get_spikepositions()

    figs_dir = '/Users/simonweber/Library/Mobile Documents/' \
               'com~apple~CloudDocs/Gridscore/figs/'
    # publication_dir = os.path.join(figs_dir, spikedata.publication)
    # filename_dir = os.path.join(publication_dir, spikedata.filename)
    # identifier_dir = os.path.join(filename_dir, spikedata.identifier)
    directory = os.path.join(figs_dir, spikedata.publication,
                             spikedata.filename,
                             spikedata.identifier)
    if not os.path.exists(directory):
        os.makedirs(directory)
    # for d in [publication_dir, filename_dir, identifier_dir]:
    # 	os.mkdir(d)

    ### Spikes with color coded grid score ###
    # fig.add_subplot(n, 1, i)
    # i += 1
    fig = plt.figure()
    shell_limits = np.array([0.9, 1.1]) * 44.6
    bins = spikedata.arena_limits[0, 1] * 2
    sigma = 8.
    spikes = Spikes(spikepositions, arena_limits=spikedata.arena_limits)
    plot = Plot(spikes)

    ### Spike map ###
    plot.spikemap()
    plt.savefig(os.path.join(directory, 'spikemap.png'))
    fig.clf()

    ### Rate map ###
    # fig.add_subplot(n, 1, i)
    # i += 1
    plot.ratemap()
    plt.savefig(os.path.join(directory, 'ratemap.png'))
    fig.clf()

    ### Correlogram ###
    # fig.add_subplot(n, 1, i)
    # i += 1
    plot.correlogram(method='sargolini')
    plt.savefig(os.path.join(directory, 'correlogram.png'))
    fig.clf()

    # ### Distances histogram ###
    print('calculating distances')
    plot.distance_histogram()
    plt.savefig(os.path.join(directory, 'distance_histogram.png'))
    fig.clf()

### Rate map from gridcells library ###
# diameter = spikedata.arena_limits[0, 1] * 2.
# arena = SquareArena(diameter, Pair2D(1., 1.))
# ax = fig.add_subplot(
# 	212, projection="gridcells_arena", arena=arena)
# plt.subplot(n, 1, i)
# i+=1
# pos = Position2D(spikes.pos_x + 90, spikes.pos_y + 90, 1.0)
# fake_spiketimes = np.arange(spikedata.spiketimes.shape[0])

# rate_map = gc_analysis.spatialRateMap(
# 	fake_spiketimes, pos, arena, sigma=50.)
# ax.spikes(fake_spiketimes, pos)
