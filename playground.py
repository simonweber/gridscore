import numpy as np

###############################################
###           Play with grid spec           ###
###############################################
# import matplotlib.pyplot as plt
# import matplotlib.gridspec as gridspec
#
# def make_ticklabels_invisible(fig):
#     for i, ax in enumerate(fig.axes):
#         ax.text(0.5, 0.5, "ax%d" % (i+1), va="center", ha="center")
#         for tl in ax.get_xticklabels() + ax.get_yticklabels():
#             tl.set_visible(False)
#
#
#
# # gridspec inside gridspec
#
# f = plt.figure()
#
# gs0 = gridspec.GridSpec(1, 2)
#
# gs00 = gridspec.GridSpecFromSubplotSpec(3, 3, subplot_spec=gs0[0])
#
# ax1 = plt.Subplot(f, gs00[:-1, :])
# f.add_subplot(ax1)
# ax2 = plt.Subplot(f, gs00[-1, :-1])
# f.add_subplot(ax2)
# ax3 = plt.Subplot(f, gs00[-1, -1])
# f.add_subplot(ax3)
#
#
# gs01 = gridspec.GridSpecFromSubplotSpec(3, 3, subplot_spec=gs0[1])
#
# ax4 = plt.Subplot(f, gs01[:, :-1])
# f.add_subplot(ax4)
# ax5 = plt.Subplot(f, gs01[:-1, -1])
# f.add_subplot(ax5)
# ax6 = plt.Subplot(f, gs01[-1, -1])
# f.add_subplot(ax6)
#
# plt.suptitle("GirdSpec Inside GridSpec")
# make_ticklabels_invisible(plt.gcf())
#
# plt.show()

###########################################################################
############## Print different psi_n for simple arrangements ##############
###########################################################################

from gridscore.spikes import get_psi_n
from gridscore.arrays import rotate_positions
reference_position = np.array([0., 0.])
# Unit vector
u = np.array([1., 0.])
### Line of points
surround_positions = np.array([
    u,
    rotate_positions([u], 180)[0]
])
### Quadratic arrangement of three points
surround_positions = np.array([
    u,
    rotate_positions([u], 90)[0],
    rotate_positions([u], 180)[0],
])
# ### Three points on hexagon 0, 60, 180
# surround_positions = np.array([
#     u,
#     rotate_positions([u], 60)[0],
#     rotate_positions([u], 180)[0],
# ])
# ### Three points on hexagon, perfect triangle
# surround_positions = np.array([
#     u,
#     rotate_positions([u], 120)[0],
#     rotate_positions([u], 240)[0],
# ])
#
# ### Six points on a hexagon, perfect hexagon
# surround_positions = np.array([
#     u,
#     rotate_positions([u], 60)[0],
#     rotate_positions([u], 120)[0],
#     rotate_positions([u], 180)[0],
#     rotate_positions([u], 240)[0],
#     rotate_positions([u], 300)[0],
# ])

# ### Three points on hexagon 0, 60, 120
# surround_positions = np.array([
#     u,
#     rotate_positions([u], 60)[0],
#     rotate_positions([u], 120)[0],
# ])

# ### A single point
# surround_positions = np.array([
#     u,
# ])

print(surround_positions)
for n in np.arange(2, 8):
    # print('n =', n)
    psiabs = np.absolute(get_psi_n(reference_position, surround_positions,
                                    n=n))
    s = '\\psiabs[{0}]'.format(n) + '{}' + ' &= {0}'.format(psiabs) + ' \\\\'
    print(s)

##########################################################################

# plot = Plot()
# intervals = np.array([
#     [0, 10], [10, 20]
# ])
# gridscores_in_intervals = np.array([
#     [4, 5], [3, 1]
# ])
# plot.plot_gridscore_in_intervals(intervals, gridscores_in_intervals)

# print_data_dictionary_escobar()
#
# a = np.random.random_sample((1000, 2))
# d = dict(positions=a)
# sio.savemat('random_positions.mat', d)
# np.save('random_positions.npy', a)

# ##########################################################################
# ####################### Data with center locations #######################
# ##########################################################################
#
# # Instead of taking the second peak in the histogram (default maximum_index = 1)
# # we take the first peak
# maximum_index = 0
# # We weight each peak in the histogram of distances between all locations
# # by the distance.
# # This make it trivial to obtain the first peak.
# weights = 'inverse_distance'
#
# # Load spikepositions for this recording
# # We need an array of shape (N, 2)
# # The array in the matlab file is of shape (2, N). We therefore take
# # the transpose.
# spikepositions = sio.loadmat('positions.mat')['positions'].T
#
# # The arena ranges from 0 to 10 along both dimensions.
# arena_limits = np.array([[0, 10], [0, 10]])
#
# # Initiating a Spikes class
# spks = spikes.Spikes(positions=spikepositions, arena_limits=arena_limits)
#
# # PSI: A comple number for each spike
# psi = spks.psi(weights=weights, maximum_index=maximum_index)
#
# # The absolute value of psi give the grid score of each spike
# psi_abs = np.absolute(psi)
#
# # The global PSI grid score is the mean of the individual scores
# global_gridscore = np.mean(psi_abs)
# print('PSI gridscore:', global_gridscore)
#
# # The direction of psi give the local orientation of the grid around each spike
# # See explanation above.
# psi_angle = np.angle(psi, deg=True) / 6
#
# # The global grid orientation is the circular mean of the local orientations
# # See explanation above.
# global_orientation = circmean(psi_angle[np.isfinite(psi_angle)],
# 							  high=30, low=-30)
# print('Global grid orientation:', global_orientation)
#
# ###############################################
# ################## Plotting ###################
# ###############################################
# plt.figure(figsize=(4, 6))
# dotsize = 7
#
# # Instantiating the Plot class
# plot = plotting.Plot(spikepositions=spikepositions, arena_limits=arena_limits)
#
# # Local grid scores
# plt.subplot(311)
# plot.spikemap(shell_limits_choice='automatic_single', weights=weights,
# 			  dotsize=dotsize, compare_to_other_symmetries=True,
# 			  maximum_index=maximum_index)
#
# # Local grid orientations
# plt.subplot(312)
# plot.spikemap(shell_limits_choice='automatic_single',
# 			  compare_to_other_symmetries=False,
# 			  color_code='psi_angle', weights=weights,
# 			  dotsize=dotsize, maximum_index=maximum_index)
#
# # Histogram of distances between all locations with highlighted shell.
# plt.subplot(313)
# plot.distance_histogram(weights=weights, maximum_index=maximum_index)
# plt.show()
